window["_fs_identity"] = function () {
  const __POIZXC__ = JSON.parse(localStorage.getItem("__POIZXC__")) ?? null
  const {customer_hash} = __POIZXC__ ?? {}
  const istcpsession = localStorage.getItem("__GFHRTY__") === "yes"
  if (customer_hash) {
    return {
      uid: customer_hash,
      displayName: customer_hash,
      source: istcpsession ? "TCP" : "Organic",
      isQminLoaded: true,
    }
  } else {
    return null
  }
}
window["_fs_debug"] = false
window["_fs_host"] = "fullstory.com"
window["_fs_script"] = "edge.fullstory.com/s/fs.js"
window["_fs_org"] = "11EGJ5"
window["_fs_namespace"] = "FS"
;(function (m, n, e, t, l, o, g, y) {
  if (e in m) {
    if (m.console && m.console.log) {
      m.console.log(
        'FullStory namespace conflict. Please set window["_fs_namespace"].',
      )
    }
    return
  }
  g = m[e] = function (a, b, s) {
    g.q ? g.q.push([a, b, s]) : g._api(a, b, s)
  }
  g.q = []
  o = n.createElement(t)
  o.async = 1
  o.crossOrigin = "anonymous"
  o.src = "https://" + window._fs_script
  y = n.getElementsByTagName(t)[0]
  y.parentNode.insertBefore(o, y)
  g.identify = function (i, v, s) {
    g(l, {uid: i}, s)
    if (v) g(l, v, s)
  }
  g.setUserVars = function (v, s) {
    g(l, v, s)
  }
  g.event = function (i, v, s) {
    g("event", {n: i, p: v}, s)
  }
  g.anonymize = function () {
    g.identify(!!0)
  }
  g.shutdown = function () {
    g("rec", !1)
  }
  g.restart = function () {
    g("rec", !0)
  }
  g.log = function (a, b) {
    g("log", [a, b])
  }
  g.consent = function (a) {
    g("consent", !arguments.length || a)
  }
  g.identifyAccount = function (i, v) {
    o = "account"
    v = v || {}
    v.acctId = i
    g(o, v)
  }
  g.clearUserCookie = function () {}
  g.setVars = function (n, p) {
    g("setVars", [n, p])
  }
  g._w = {}
  y = "XMLHttpRequest"
  g._w[y] = m[y]
  y = "fetch"
  g._w[y] = m[y]
  if (m[y])
    m[y] = function () {
      return g._w[y].apply(this, arguments)
    }
  g._v = "1.3.0"
})(window, document, window["_fs_namespace"], "script", "user")
;(function () {
  "use strict" //eslint-disable-line
  function fs(api) {
    if (!window._fs_namespace) {
      console.error(
        'FullStory unavailable, window["_fs_namespace"] must be defined',
      )
      return undefined
    } else {
      return api
        ? window[window._fs_namespace][api]
        : window[window._fs_namespace]
    }
  }

  function waitUntil(predicateFn, callbackFn, timeout, timeoutFn) {
    let totalTime = 0
    let delay = 64

    const resultFn = function () {
      if (typeof predicateFn === "function" && predicateFn()) {
        callbackFn()
        return
      }
      delay = Math.min(delay * 2, 1024)
      if (totalTime > timeout) {
        if (timeoutFn) {
          timeoutFn()
        }
      }
      totalTime += delay
      setTimeout(resultFn, delay)
    }
    resultFn()
  }
  const timeout = 2000

  function identify() {
    if (typeof window._fs_identity === "function") {
      const userVars = window._fs_identity()
      if (typeof userVars === "object" && typeof userVars.uid === "string") {
        fs("setUserVars")(userVars)
        fs("restart")()
      } else {
        fs("log")(
          "error",
          "FS.setUserVars requires an object that contains uid",
        )
      }
    } else {
      fs("log")("error", 'window["_fs_identity"] function not found')
    }
  }
  fs("shutdown")()
  waitUntil(window._fs_identity, identify, timeout, fs("restart"))
})()
