## Application Flow

- index.html
- index.js
- App.js
- RouteConfig.js
- PageRoute.js
- PrivateRoute.js (Not much important)
- Any page from pages folder
  - index.js (How component and functionality is configured)
  - styles.js (How styled component is being used)

## New Page

- Create Folder in page name
- Create index.js(For Functionality nd HTML rendering) and styles.js(For css)
- Create Path name in RouteConstants file
- Register Page in Route config

## Stack overview

- [React]
- [Styled Components] - Styling React Components and Custom CSS
- [Create React App] - Boilerplate

## To apply a title to the HTML page

```sh
Open `RouteConfig.js` and pass the value under `title` parameter in the `data` parameter

Sample example:
data={{
  title: 'My Profile',
  ...rest
}}
```

## To use a color

```sh
Open `src/index.css` and check whether the hex-code or rgb value is present in the file or not
Copy the CSS variable and use as below

For hex values:
.container {
  background-color: var(--hex-one)
}

For rgb values:
.container {
  background-color: rgb(var(--rgb-one), var(--alpha-one))
}
```

## To apply Proxima Nova Bold font

```sh
1. [h1 - h6], <a />, <button />, <input /> tags has Proxima Nova Bold font by default
2. To apply this font anyway, use `.bold` class in any element you wish
```

## To apply Proxima Nova Light font

```sh
To apply this font anyway, use `.light` class in any element you wish
```

## To apply Calibri font

```sh
1. This font is applied to all the elements by default except the `[h1 - h6], <a />, <button />, <input />` tags.
2. To apply this font anyway, use `.regular` class in the HTML element
```

## To apply Signature font

```sh
Use `.signature` class in the HTML element
```

## To apply Ripple effect to a button HTML tag

```sh
import withRipple from 'hooks/withRipple'

// To use it,
const handleClick = (event) => {
  withRipple(function(){
    // Your button click changes
  }, event)
  // Make sure to pass the correct event object.
}

return (
  <button onClick={handleClick} />
)
```

## To use an image(if it is not coming from an API service)

1. File format must be a `PNG`
2. File name must be in lowercase and hyphenated convention. Eg.
   `your-image.png`
3. File size must be compressed before using in the project
4. Place the `your-image.png` file inside `src/assets/images` directory

```sh
import sample from 'assets/images/your-image.png'

const Component = () => {
  return (
    <img src={sample} alt='sample' />
  )
}
```

## To use an icon

1. File format must be a SVG
2. File name must be in lowercase and hyphenated convention. Eg. `your-icon.svg`
3. File size must be a maximum of `1 KB`. Check the size before you use in
   project
4. Place the `your-icon.svg` file inside `src/assets/icons` directory
5. Open `your-icon.svg` and change the `mask0` attribute values to
   `mask-your-icon-name`

```sh
import {ReactComponent as YourIcon} from 'assets/icons/your-icon.svg'

const Component = () => {
  return (
    <YourIcon />
  )
}
```

## useModal() hook

```sh
import useModal from "hooks/useModal"
```

Import inside the React component

```sh-
const {toggleModal} = useModal()
```

To open the modal, call

```sh-
toggleModal({
  message: "You were successful",
  primaryButtonText: "Okay",
  onPrimaryClick: handlePrimaryClick,
})
```

To close the modal, call

```sh-
  toggleModal()
```

## useAuth() hook

```sh
import useAuth from "hooks/useAuth"
```

Import inside the React component

```sh-
const auth = useAuth()
```

To sign in, call

```sh-
auth.signin(function () {
    // callback
    console.log("Login successful")
})
```

To sign out user, call

```sh-
  auth.signout()
```

## How to use Static header which has back button, title header and app logo ?

```sh
Open `RouteConfig.js` where all the routes of the web-app are defined
```

Pass details in `data` parameter as a `prop` to `PageRoute`

```sh-
data={{
  header: {text: "My Orders", logo: true}
}}
```

Details of props passed

```sh-
text - String - The heading to be shown in the title header
logo - Boolean - Hides/Shows the qmin app logo which comes on the right side of the header
textInvert - Boolean - Changes the color of text to white
backInvert - Boolean - Changes the color of back icon to white
logoInvert - Boolean - Changes the color of logo icon to white
```

## Custom header using useHeader() hook

```sh
import useHeader from "hooks/useHeader"
```

Import inside the React component

```sh-
const {toggleHeader} = useHeader()
```

To render the header with your component, call

```sh-
toggleHeader({custom: <Component />, fixed: true/false})
```

To cleanup custom header, call

```sh-
  toggleHeader() // if you pass nothing to this function, header gets dismissed
```

## Custom footer using useFooter() hook

```sh
import useFooter from "hooks/useFooter"
```

Import inside the React component

```sh-
const {toggleFooter} = useFooter()
```

To render the footer with your component, call

```sh-
toggleFooter({custom: <Component />, fixed: true/false})
```

To cleanup custom footer, call

```sh-
  toggleFooter() // if you pass nothing to this function, footer gets dismissed
```

---

## Mandatory guidelines

### 1. Branching

```sh-
Branch name should either be created from JIRA tasks or it should be a meaningful name.

##### Correct usage

`feature/LOYALTY-1234-integrate-loyalty-points`, `feature/AUTH-1234-integrate-otp-login-flow`

##### Incorrect usage

`feature/spike-webview-10-May`

```

### 2. Use JIRA IDs in commit message and follow commit patterns

```sh-
git commit -m "LOYALTY-1234: Add a test case for the sample component"
git commit -m "TCPAPP-1234: Integrate API for Sample component"


Every commit message should in active case. Must not be greater than 72 characters. Should
summarize the code changes in commit. After a newline, it should have JIRA ticket number
followed by task description.
Reference: https://chris.beams.io/posts/git-commit

Rules for committing:

1. Separate subject from body with a blank line
2. Limit the subject line to 50 characters
3. Capitalize the subject line
4. Do not end the subject line with a period
5. Use the imperative mood in the subject line
6. Wrap the body at 72 characters
7. Use the body to explain what and why vs. How
```

### 3. Use JIRA IDs with proper description in MR title and description

### 4. Attach a screenshot for test cases in the MR

### 5. Avoid making large pull requests so that code review gets easier

### 6. Use semantic HTML elements as per W3C standards

Correct usage

`<section>Hey this is a sample section</section>` for sections
`<article>Hey this is a sample section</article>` for articles
`<p>Hey this is a sample section</p>` for paragraphs
`<main>Hey this is a sample section</main>` for the content to be rendered

Not a very correct usage

`<div>Hey this is a sample section</div>`

### 7. Meaningful Identifiers. Nomenclature should be relevant

### 8. Prefer ES6 functions strictly over conventional methods

### 9. Perform a self code-review to ensure sanity of your feature or hotfix

### 10. If using JEST and react testing library, write your test cases and check coverage before committing your code.

### 11. Check for logs and remove them if not required

### 12. Fix eslint-issues on the fly. Remove the paths from .eslintignore files and work on fixing the eslint issues for your owned components.

### 13. Use npm run build in local to see whether your build is getting generated successfully or not

### 14. Lazy Loading of React Components

```sh-
Use React.lazy along with React.Suspense.

You can use this loader - https://statddevdecipwalrs02.z29.web.core.windows.net/?path=/story/loader--custom-loader
```

### 15. Lazy loading of images through LazyImage Component

```sh-
<LazyImage src="path.png" alt="alternative" />
```

### 16. Destructure objects and arrays

```sh-
Let us say that if data is used more than once, then no need to do reponse.data multiple times. Instead destructure it from the source object/array.
const {data} = object
const [data] = array
```

### 17. Avoid nested destructuring of Objects and arrays

```sh-
const {a: {b}} = data // BAD PRACTICE
const {a} = data
// Use data.b simply
```

### 18. Avoid nested rendering and make separate functions for nested ternary ops

```sh-
const renderHeader = () => {
    return condition2 ? TrueValue : FalseValue
}
return (
    <div>
        {
            condition1 ? renderHeader() : null
        }
    <div>
)
```

### 19. Avoid inline functions as they are a perf hit. These functions get initialized on every re-render anonymously. As they get initialised multiple times, they consume memory every time. Use callbacks instead of inline functions.

```sh-
const renderHeader = () => {
    return condition2 ? TrueValue : FalseValue
}
return (
    <div onClick={()=>{console.log('some value')}}>Some inner text<div> // BAD PRACTICE
)
```

```sh-
const renderHeader = () => {
    console.log('some value')
}
return (
    <div onClick={renderHeader}>Some inner text<div> // GOOD PRACTICE
)
```

### 20. Use data attribute for passing values in callback function

```sh-
const renderHeader = (e) => {
    const {value} = e.dataset
    const {value} = e.current.dataset // In case of current target...
    console.log(value) // Prints 11111
}
return (
    <div onClick={renderHeader} data-value={1111}>Some inner text<div> // GOOD PRACTICE
)
```

### 21. Use Functional Components only. Use Memo for avoiding re-renders

```sh-
const Component = (
    <div>Some inner text<div> // GOOD PRACTICE
)
export default React.memo(Component) // Memoize to avoid re-renders
```

### 22. Use try-catch when writing complex JS logic

```sh-
const executeExpensive = () => {
    try{
     // Expensive calcculations
    }
    catch(e) {
        console.log(e) // eslint-disable-line
    }
}
export default React.memo(Component) // Memoize to avoid re-renders
```

### 23. If something is common ,then take it in a one variable then use this variable

Instead of using props.url everytime

```sh-
const {url} = props

return (
    <div>{props.url}</div>
    <span>{props.url}</span>
    <section>{props.url}</section>
)
```

Destructure

```sh-
const {url} = props

return (
    <div>{url}</div>
    <span>{url}</span>
    <section>{url}</section>
)
```

### 24. Use default values for props in React Components

```sh-
const Component = ({
    a=0,
    b=false,
    c="Loading..."
}) => {
    return (
        <div>
            <div>{a}</div>
            <div>{b}</div>
            <div>{c}</div>
        </div>
    )
}
```

### 25. Trigger an Analytics event

1. Declare the function and It's parameters in it's arguments
2. Call that Analytic function inside your React component

```sh-
export const errorMessageEvent = (name, desc) => {
  const data = {
    error_msg_name: name,
    error_message_description: desc,
  }
  registerAnalytics("error_msg", data)
}
```

```sh-
import {errorMessageEvent} from "analytics/QminAnalytics"

errorMessageEvent("Order failder", "Order failed due to mismatch in Cart value")
```

### 26. useResize Hook

1. To observe the size of window to show the responsive UI to the user
2. Load component's lazily depends on the Boolean value the hook returns

```sh-
import useResize from "hooks/useResize"

const DesktopHeader = lazy(() =>
  import(
    /* webpackChunkName: "desktop-header" */ "components/header/desktop-header"
  ),
)
const MobileHeader = lazy(() =>
  import(
    /* webpackChunkName: "mobile-header" */ "components/header/mobile-header"
  ),
)

 const {isDesktop} = useResize()

 return isDesktop ? (
    <DesktopHeader
      showTrackIcon={activeOrders.length > 0}
      deliveryLoc={deliveryLoc}
      authUser={authUser}
    />
  ) : (
    <MobileHeader
      showTrackIcon={activeOrders.length > 0}
      deliveryLoc={deliveryLoc}
      authUser={authUser}
    />
  )
```
