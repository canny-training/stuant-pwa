export const transitionTimeout = "600"
export const rippleTimeout = "200"
export const getAmount = (amount, isDecimal = false) =>
  amount
    .toFixed(isDecimal ? 2 : 0)
    .toString()
    .replace(/\B(?=(\d{3})+(?!\d))/g, ",")
export const googleScriptSrc = `https://maps.googleapis.com/maps/api/js?key=${process.env.REACT_APP_GOOGLE_API_KEY}&libraries=places`
export const CALLBACK_MESSAGE =
  "Your call-back request has been successfully submitted. Our customer support team will contact you at the earliest."
export const alertQuery = category =>
  `Are you sure you want to request a call back for issues related with ${category}?`
export const isValidName = value => {
  let key = value.charCodeAt(value.length - 1)
  return (
    (key >= 65 && key <= 90) ||
    (key >= 97 && key <= 122) ||
    (key === 32 && value.length !== 1) ||
    value === ""
  )
}
export const MOBILE_WIDTH = 760
export const DESKTOP_WIDTH = `(min-width: ${MOBILE_WIDTH}px)`
export const getPageURL = () =>
  window.location.pathname + window.location.search
