export const URL_USER_DATA = "auth/me"
export const URL_LOGIN_TOKEN = "auth/login/token"
export const URL_STUDENTS = "students"
export const URL_BOARDS = "boards"
export const URL_CLASSES = "classes"
export const URL_TESTS = "tests"
export const UPDATE_USER = "auth/update-user"

//TALENT
export const TALENT_MODULE = "talent-management"
export const URL_TALENT_CATEGORIES = `${TALENT_MODULE}/categories`
export const URL_TALENT_CERTIFICATE = `${TALENT_MODULE}/certifications`
export const URL_TALENT_EVENT = `${TALENT_MODULE}/events`
export const URL_CREATE_TALENT_PROFILE = `${TALENT_MODULE}/profiles`
export const talentProfile = profileId =>
  `${URL_CREATE_TALENT_PROFILE}/${profileId}`
export const talentProfileImage = profileId =>
  `${talentProfile(profileId)}/picture`
export const eventById = eventId => `${URL_TALENT_EVENT}/${eventId}`
export const certificateById = certificateId =>
  `${URL_TALENT_CERTIFICATE}/${certificateId}`
export const postEventAttachment = eventId =>
  `${eventById(eventId)}/attachments`
export const postCertificateAttachment = certificateId =>
  `${certificateById(certificateId)}/attachments`
export const talentById = (talentId, profileId) =>
  `${talentProfile(profileId)}/talents/${talentId}`
export const profileTalents = profileId =>
  `${URL_CREATE_TALENT_PROFILE}/${profileId}/talents?showRecent=2`
export const getSubCategory = subCategoryId =>
  `${TALENT_MODULE}/sub-categories/${subCategoryId}`
export const deleteTalentAttachment = attachmentId =>
  `${TALENT_MODULE}/assets/${attachmentId}`
export const addRecognitionImages = eventId =>
  `${eventById(eventId)}/recognitions`

export const customTests = studentId => `tests?studentId=${studentId}`
export const subjectCategories = (studentId, classId, boardId) =>
  `categories?classId=${classId}&boardId=${boardId}&studentId=${studentId}`
export const overAllReport = studentId =>
  `students/${studentId}/reports/overall`
export const studentById = studentId => `students/${studentId}`
export const studentProfile = studentId =>
  `${studentById(studentId)}/profile-picture`
export const getTestById = (studentId, testId) =>
  `students/${studentId}/tests/${testId}`
export const getChapter = (studentId, chapterId) =>
  `chapters/${chapterId}?studentId=${studentId}`
export const getGeneralCategory = (classId, boardId) =>
  `categories?classId=${classId}&boardId=${boardId}`
export const getSubjectURL = (studentId, subId) =>
  `subjects/${subId}?studentId=${studentId}`
export const getReportByCategory = (studentId, categoryId) =>
  `students/${studentId}/reports/classes-subjects?categoryId=${categoryId}`
export const getReportBySubject = (studentId, subjectId) =>
  `students/${studentId}/reports/classes-subjects/${subjectId}/student-tests`
export const getReportBySubjectDetail = (studentId, subjectId) =>
  `students/${studentId}/reports/classes-subjects/${subjectId}`
export const getCustomizedTestReport = (studentId, classId, subjectId) =>
  `students/${studentId}/reports/classes-subjects/${subjectId}/custom-tests?classId=${classId}`
export const getChapterTestReport = (studentId, classId, subjectId) =>
  `students/${studentId}/reports/classes-subjects/${subjectId}/student-tests?classId=${classId}`
export const getQuestionsByChapter = chapterId =>
  `/chapters/${chapterId}/questions`
export const getQuestionsByTest = (studentId, testId) =>
  `students/${studentId}/tests/${testId}/start`
export const postTestAnswer = (studentId, studentTestId) =>
  `students/${studentId}/answers/${studentTestId}/import`
export const completeTest = (studentId, studentTestId) =>
  `students/${studentId}/answers/${studentTestId}/complete`
export const resumeTest = (studentId, studentTestId) =>
  `students/${studentId}/student-tests/${studentTestId}`

export const getCategories = (studentId, quarterId, year) =>
  `parental-analysis/categories?studentId=${studentId}&quarter=${quarterId}&year=${year}`
export const getQuesAndAns = (categoryId, studentId, quarterId, year) =>
  `parental-analysis/categories/${categoryId}?studentId=${studentId}&quarter=${quarterId}&year=${year}`
export const postParentalAnswer = analysisId =>
  `parental-analysis/${analysisId}/answer`
export const completeRating = analysisId =>
  `parental-analysis/${analysisId}/complete`
export const getHistory = (categoryId, studentId, year) =>
  `parental-analysis/categories/${categoryId}/history?studentId=${studentId}&year=${year}`

export const GET_PLANS = "subscriptions/plans"
export const INITIATE_PAYMENT = "subscriptions/initiate-payment"
export const cancelsubscription = subscriptionId =>
  `subscriptions/${subscriptionId}/cancel`
export const RP_INITIATE_PAYMENT = "subscriptions/razorPay/initiate-payment"

export const MODULE_SCIENTIFIC = "scientific-analysis"
export const getScientificCategories = (studentId, quarterId, year) =>
  `${MODULE_SCIENTIFIC}/categories?studentId=${studentId}&quarter=${quarterId}&year=${year}`
export const getScientificCategory = (categoryId, studentId, quarterId, year) =>
  `${MODULE_SCIENTIFIC}/categories/${categoryId}?studentId=${studentId}&quarter=${quarterId}&year=${year}`
export const postScientificAnswer = analysisId =>
  `${MODULE_SCIENTIFIC}/${analysisId}/answers`
export const completeScientificRating = analysisId =>
  `${MODULE_SCIENTIFIC}/${analysisId}/complete`
export const getScientificReport = (categoryId, studentId, year) =>
  `${MODULE_SCIENTIFIC}/categories/${categoryId}/history?studentId=${studentId}&year=${year}`

export const POST_FEEDBACK = "app-feedbacks"

// * COMPETITIONS
export const MODULE_COMPETITIONS = "competitions"
export const getAllCompetitions = (classId, boardId, studentId) =>
  `${MODULE_COMPETITIONS}?classId=${classId}&boardId=${boardId}&studentId=${studentId}&include=prizes,levels,prizes.participants`
export const getCompetitionById = (competitionId, studentId) =>
  `${MODULE_COMPETITIONS}/${competitionId}?include=prizes,levels,sponsors,prizes.participants,prizes.participants.student,levels.test,sponsors.organization&studentId=${studentId}`
export const addParticipant = competitionId =>
  `${MODULE_COMPETITIONS}/${competitionId}/participants`
export const retryPayment = (competitionId, participantId) =>
  `${MODULE_COMPETITIONS}/${competitionId}/participants/${participantId}/retry-payment`
