const __POIZXC__ = JSON.parse(localStorage.getItem("__POIZXC__")) ?? null
const {phone, email, customer_hash} = __POIZXC__ ?? {}
const loginStatus = __POIZXC__ ? 1 : 0

export const scrollItemEvent = ({
  id,
  dish_type,
  title,
  category,
  price,
  restaurant_name,
  item_ref_id,
}) => {
  const data = {
    item_id: id,
    item_name: title,
    item_category: category,
    item_brand: "",
    item_price: price,
    hotel_name: restaurant_name,
    food_type: dish_type,
    item_list_name: "",
    item_list_id: item_ref_id,
    favourite_food: "null",
    membership_id: "null",
    mobile_number: phone,
    membership_type: "null",
    email_address: email,
    user_id: customer_hash,
    screen_name: "home",
    login_status: loginStatus,
    platform: "web",
  }
  registerAnalytics("view_item_list", data)
}

export const viewItemEvent = ({price}) => {
  const data = {
    currenct: "INR",
    item_price: price,
    favourite_food: "null",
    membership_id: "null",
    mobile_number: phone,
    membership_type: "null",
    email_address: email,
    user_id: customer_hash,
    screen_name: "home",
    login_status: loginStatus,
    platform: "web",
  }
  registerAnalytics("view_item", data)
}

export const selectItemEvent = (screen, {title, id}) => {
  const data = {
    item_list_name: title,
    item_list_id: id,
    favourite_food: "null",
    membership_id: "null",
    mobile_number: phone,
    membership_type: "null",
    email_address: email,
    user_id: customer_hash,
    screen_name: screen,
    login_status: loginStatus,
    platform: "web",
  }
  registerAnalytics("select_item", data)
}

export const addCartEvent = ({price, quantity}, screen) => {
  const data = {
    currency: "INR",
    value: price,
    quantity: quantity,
    favourite_food: null,
    membership_id: null,
    mobile_number: phone,
    membership_type: null,
    email_address: email,
    user_id: customer_hash,
    screen_name: screen,
    login_status: loginStatus,
    platform: "web",
  }
  registerAnalytics("add_to_cart", data)
}

export const removeCartEvent = ({price, quantity}, screen) => {
  const data = {
    currency: "INR",
    value: price,
    quantity: quantity,
    favourite_food: "null",
    membership_id: "null",
    mobile_number: phone,
    membership_type: "null",
    email_address: email,
    user_id: customer_hash,
    screen_name: screen,
    login_status: loginStatus,
    platform: "web",
  }
  registerAnalytics("remove_from_cart", data)
}

export const beginCheckoutEvent = screen => {
  const data = {
    currency: "INR",
    coupon: "null",
    favourite_food: "null",
    membership_id: "null",
    mobile_number: phone,
    membership_type: "null",
    email_address: email,
    user_id: customer_hash,
    screen_name: screen,
    login_status: loginStatus,
    platform: "web",
  }
  registerAnalytics("begin_checkout", data)
}

export const payNowEvent = selOffer => {
  const data = {
    currency: "INR",
    coupon: selOffer?.promotion_code || "",
    favourite_food: "null",
    membership_id: "null",
    mobile_number: phone,
    membership_type: "null",
    email_address: email,
    user_id: customer_hash,
    screen_name: "order_summary",
    login_status: loginStatus,
    platform: "web",
  }
  registerAnalytics("pay_now", data)
}

export const purchaseEvent = orderdetails => {
  const {payment, total_amount, charges, offer_type} = orderdetails ?? {}
  const data = {
    transaction_id: payment?.[0]?.txn_id,
    affiliation: "null",
    currency: "INR",
    value: total_amount,
    tax: charges?.[0]?.amount,
    shipping: 0,
    coupon: offer_type,
    favourite_food: "null",
    membership_id: "null",
    mobile_number: phone,
    membership_type: "null",
    email_address: email,
    user_id: customer_hash,
    screen_name: "Home",
    login_status: loginStatus,
    platform: "web",
  }
  registerAnalytics("purchase", data)
}

export const foodRatingEvent = item => {
  const data = {
    food_rating: item,
    favourite_food: "null",
    membership_id: "null",
    mobile_number: phone,
    membership_type: "null",
    email_address: email,
    user_id: customer_hash,
    screen_name: "order_details",
    login_status: loginStatus,
    platform: "web",
  }
  registerAnalytics("food_rating", data)
}

export const loginEvent = item => {
  const data = {
    favourite_food: "null",
    membership_id: "null",
    mobile_number: phone,
    membership_type: "null",
    email_address: email,
    user_id: customer_hash,
    screen_name: "enter_otp",
    login_status: loginStatus,
    platform: "web",
  }
  registerAnalytics("login", data)
}

export const interactionDeliveryEvent = item => {
  const data = {
    favourite_food: "null",
    membership_id: "null",
    mobile_number: phone,
    membership_type: "null",
    email_address: email,
    user_id: customer_hash,
    screen_name: "order_schedule",
    login_status: loginStatus,
    platform: "web",
  }
  registerAnalytics("interaction_delivery_widget", data)
}

export const appScreenNameEvent = screen => {
  const data = {
    favourite_food: "null",
    membership_id: "null",
    mobile_number: phone,
    membership_type: "null",
    email_address: email,
    user_id: customer_hash,
    screen_name: screen,
    login_status: loginStatus,
    platform: "web",
  }
  registerAnalytics("app_screen_name", data)
}

export const locationWidgetEvent = item => {
  const data = {
    favourite_food: "null",
    membership_id: "null",
    mobile_number: phone,
    membership_type: "null",
    email_address: email,
    user_id: customer_hash,
    screen_name: "home",
    login_status: loginStatus,
    platform: "web",
  }
  registerAnalytics("interaction_location_widget", data)
}

export const appLaunchEvent = () => {
  const data = {
    favourite_food: "null",
    membership_id: "null",
    mobile_number: phone,
    membership_type: "null",
    email_address: email,
    user_id: customer_hash,
    screen_name: "home",
    login_status: loginStatus,
    platform: "web",
  }
  registerAnalytics("app_launched", data)
}

export const errorMessageEvent = (name, desc) => {
  const data = {
    error_msg_name: name,
    error_message_description: desc,
  }
  registerAnalytics("error_msg", data)
}

export const successMsg = orderdetails => {
  const {
    order_id,
    order_status,
    payment,
    total_count,
    total_amount,
    charges,
    tax,
    discount_amount,
  } = orderdetails ?? {}
  const data = {
    order_id_str: order_id,
    order_status_str: order_status,
    payment_method_str: payment?.[0]?.payment_method_type,
    items_int: total_count,
    revenue_real: total_amount,
    shipping_real: charges?.[0]?.amount,
    tax_real: tax?.total_amount,
    discount_real: discount_amount,
  }
  registerAnalytics("FSTDL Revenue", data)
}

const registerAnalytics = (eventName, eventData) => {
  window.FS?.event(eventName, eventData)
}
