import {useEffect} from "react"
import {Route} from "react-router-dom"
import useAxios from "hooks/useAxios"
import useOffline from "hooks/useOffline"
import RouteConfig from "routes/RouteConfig"

const App = () => {
  const {setOffline} = useOffline()
  const axios = useAxios()
  useEffect(() => {
    axios.interceptors.response.use(
      res => res,
      async err => {
        setOffline(err?.response?.status?.toString().startsWith("5"))
        return Promise.reject(err)
      },
    )
  }, []) //eslint-disable-line

  return (
    <div className="App">
      <Route render={({location}) => <RouteConfig location={location} />} />
    </div>
  )
}
export default App
