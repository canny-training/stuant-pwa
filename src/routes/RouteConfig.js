import {Suspense, lazy} from "react"
import {Switch} from "react-router-dom"
import PageRoute from "./PageRoute"
import PageLoader from "components/Loader/PageLoader"
import {
  PAGE_DASHBOARD,
  PAGE_LANDING,
  PAGE_LOGIN,
  PAGE_OTP,
  PAGE_SPLASH,
} from "constants/RouteConstants"

const Splash = lazy(() =>
  import(/* webpackChunkName:"splash" */ "pages/splash"),
)
const Landing = lazy(() =>
  import(/* webpackChunkName:"landing-tour" */ "pages/landing"),
)
const Dashboard = lazy(() =>
  import(/* webpackChunkName:"dashboard" */ "pages/dashboard"),
)
const Login = lazy(() => import(/* webpackChunkName:"login" */ "pages/login"))
const OTP = lazy(() => import(/* webpackChunkName:"otp" */ "pages/otp"))
const RouteConfig = ({location}) => {
  return (
    <Suspense fallback={<PageLoader show={true} />}>
      <Switch location={location}>
        <PageRoute
          exact
          path={PAGE_SPLASH}
          data={{
            title: "STUANT - To analyze the student completely",
            isPrivate: true,
          }}
        >
          <Splash />
        </PageRoute>
        <PageRoute
          exact
          path={PAGE_LOGIN}
          data={{
            title: "STUANT Sign-in",
            isPrivate: false,
          }}
        >
          <Login />
        </PageRoute>
        <PageRoute
          exact
          path={PAGE_OTP}
          data={{
            title: "Verify One Time Password",
            isPrivate: false,
          }}
        >
          <OTP />
        </PageRoute>
        <PageRoute
          exact
          path={PAGE_LANDING}
          data={{
            title: "App Tour",
            isPrivate: false,
          }}
        >
          <Landing />
        </PageRoute>
        <PageRoute
          exact
          path={PAGE_DASHBOARD}
          data={{
            title: "Home",
            isPrivate: false,
          }}
        >
          <Dashboard />
        </PageRoute>
      </Switch>
    </Suspense>
  )
}
export default RouteConfig
