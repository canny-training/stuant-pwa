import {useEffect, memo, useState} from "react"
import {Route} from "react-router-dom"
import useHeader from "hooks/useHeader"
import PrivateRoute from "./PrivateRoute"
import PageLoader from "components/Loader/PageLoader"

const PageRoute = ({data = {}, ...rest}) => {
  const {
    isPrivate = false,
    header,
    title = "STUANT - App for Students Transformation",
  } = data
  const {toggleHeader} = useHeader()
  const [isLoading] = useState(false)
  const {title: docTitle} = document
  if (docTitle !== title) document.title = title

  useEffect(() => {
    // sets up the static header on route change
    if (header) toggleHeader(header)
  }, [header]) //eslint-disable-line

  const Component = isPrivate ? PrivateRoute : Route
  if (isLoading) return <PageLoader show={true} />
  return <Component {...rest} />
}
export default memo(PageRoute)
