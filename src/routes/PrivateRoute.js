import useUser from "hooks/useUser"
import {Route, Redirect} from "react-router-dom"
import {PAGE_LOGIN} from "constants/RouteConstants"

const PrivateRoute = ({children, ...rest}) => {
  const {isSignedIn} = useUser()
  return (
    <Route
      {...rest}
      render={({location}) =>
        isSignedIn ? (
          children
        ) : (
          <Redirect
            to={{
              pathname: PAGE_LOGIN,
              state: {from: location},
            }}
          />
        )
      }
    />
  )
}

export default PrivateRoute
