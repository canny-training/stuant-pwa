import {URL_LOGIN_TOKEN} from "constants/api-constants"
import useAxios from "hooks/useAxios"

const useLoginAPI = () => {
  const axios = useAxios()
  const loginWithFirebase = async () =>
    await axios({url: URL_LOGIN_TOKEN, method: "get"})

  return {
    loginWithFirebase,
  }
}

export default useLoginAPI
