// import { URL_STUDENTS } from "constants/api-constants"

// const { default: useAxios } = require("hooks/useAxios")

// const useStudent = () => {
//   const axios = useAxios()
//   const createStudent = async (studentData) => axios({ url: URL_STUDENTS, data: studentData, method: "post" })

//   const updateStudent = (studentData) => {
//   ApiHelper.updateRequest(activity, APIConstants.studentById(Common.getCurrentStudent(activity).studentId!!), studentObject) { json, volleyError ->
//       volleyError?.let { callBack(null, DataError(it.message)) }

//       json?.let {
//           val cStudent = Common.getCurrentStudent(activity)
//           val student = Gson().fromJson(it, StudentData::class.java)
//           cStudent.board = student.board
//           cStudent.`class` = student.`class`
//           cStudent.preferences = student.preferences
//           cStudent.updatedAt = cStudent.updatedAt
//           callBack(cStudent, null)
//       }
//   }
// }

// export const updateStudentInfo(studentData: AddStudentData, callBack: OnCompleteListener<StudentData>) {
//   val cStudent = Common.getCurrentStudent(activity)
//   val studentObject = JSONObject()
//   studentObject.put("firstName" , studentData.firstName)
//   if (studentData.lastName != null && studentData.lastName!!.isNotEmpty()) studentObject.put("lastName" , studentData.lastName)
//   if (studentData.email != null && studentData.email!!.isNotEmpty()) studentObject.put("email", studentData.email)
//   ApiHelper.updateRequest(activity, APIConstants.studentById(cStudent.studentId!!), studentObject) { json, volleyError ->
//       volleyError?.let { callBack(null, DataError(it.message)) }
//       json?.let {
//           if (studentData.profilePicture != null) {
//               updateStudentProfile(activity, studentData.profilePicture!!, cStudent.studentId, callBack)
//           } else {
//               val student = Gson().fromJson(json, StudentData::class.java)
//               cStudent.firstName = student.firstName
//               cStudent.lastName = student.lastName
//               cStudent.email = student.email
//               cStudent.dateOfBirth = student.dateOfBirth
//               cStudent.gender = student.gender
//               cStudent.profilePicture = student.profilePicture
//               cStudent.updatedAt = cStudent.updatedAt
//               Common.updateSingleton(cStudent)
//               callBack(cStudent, null)
//           }
//       }
//   }
// }

// export const updateStudentProfile(image: Uri, studentId: Int, callBack: OnCompleteListener<StudentData>) {
//   val filePath = FilePath.getPath(activity, image)

//   val imageObject = HashMap<String, ArrayList<VolleyMultipartHandler.DataPart>>()
//   filePath?.let {
//       imageObject["attachment"] = arrayListOf(VolleyMultipartHandler.DataPart(
//           Common.getFileName(filePath),
//           Common.getFileBytes(filePath),
//           Common.getFileType(filePath)))
//   }

//   multipartRequest(activity, APIConstants.studentProfile(studentId), imageObject, null, Request.Method.PUT) { jsonResponse, dataError ->
//       if (jsonResponse != null) {
//           val cStudent = Common.getCurrentStudent(activity)
//           val student = Gson().fromJson(jsonResponse, StudentData::class.java)
//           cStudent.firstName = student.firstName
//           cStudent.lastName = student.lastName
//           cStudent.email = student.email
//           cStudent.dateOfBirth = student.dateOfBirth
//           cStudent.gender = student.gender
//           cStudent.profilePicture = student.profilePicture
//           cStudent.updatedAt = cStudent.updatedAt
//           Common.updateSingleton(cStudent)
//           callBack(cStudent, null)
//       } else {
//           callBack(null, dataError)
//       }
//   }
// }

// }

// export default useStudent()
