import {URL_LOGIN_TOKEN} from "constants/api-constants"
import useAxios from "hooks/useAxios"

const axios = useAxios()
export const loginWithFirebase = async () => {
  try {
    const response = await axios({url: URL_LOGIN_TOKEN})
    return Promise.resolve(response)
  } catch (error) {
    return Promise.reject(error)
  }
}

// export const getSignedInUserData(callBack: OnCompleteListener<UserData>) {
//   ApiHelper.getRequest(activity, APIConstants.URL_USER_DATA) { jsonObject, volleyError ->
//       jsonObject?.let {
//           val data = Gson().fromJson(it, TokenData::class.java)
//           data.access_token?.let { token ->
//               if (token.isNotEmpty()) UserSingleton.getInstance()?.tokenData?.access_token = token
//           }

//           data.user?.let { user -> UserSingleton.getInstance()?.tokenData?.user = user }

//           data.metaData?.let { meta -> UserSingleton.getInstance()?.tokenData?.metaData = meta }
//           callBack(data.user, null)
//       } ?: callBack(null, DataError(volleyError?.message))
//   }
// }

// export const updateUser(activity: Context,requestJson: JSONObject, callBack: OnCompleteListener<UserData>) {
//   ApiHelper.updateRequest(activity, UPDATE_USER, requestJson) { jsonObject, volleyError ->
//       jsonObject?.let {
//           val data = Gson().fromJson(it, UserData::class.java)
//           UserSingleton.getInstance()?.tokenData!!.user = data!!
//           callBack(data, null)
//       } ?: callBack(null, DataError(volleyError?.message))
//   }
// }
