import {useContext} from "react"
import PageLoaderContext from "contexts/PageLoaderContext"

const usePageLoader = () => useContext(PageLoaderContext)
export default usePageLoader
