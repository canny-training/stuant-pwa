import {rippleTimeout} from "constants/constants"

/**
 * @description Executes on button, list item element only for ripple effect
 * Use this function to execute your callback after a ripple effect
 * @usage withRipple(function(){}, event)
 * where first argument is the callback to be executed and
 * second argument is the event of the clicked element.
 * This function will find the "target" object inside the "event" object.
 * Make sure to pass the correct event object.
 */
const initiateRipple = event =>
  new Promise((resolve, reject) => {
    const clickedElement = event.target
    const isDivision = clickedElement.tagName === "DIV"
    const isButton = clickedElement.type === "submit"
    const isListItem = clickedElement.tagName === "LI"

    // For button, Add more elements if needed in future
    if (isDivision || isButton || isListItem) {
      // Hard setting the style for division element
      if (isDivision) {
        clickedElement.style.position = "relative"
        clickedElement.style.overflow = "hidden"
      }
      const circle = document.createElement("span")
      const diameter = Math.max(
        clickedElement.clientWidth,
        clickedElement.clientHeight,
      )
      const radius = diameter / 2
      circle.style.width = circle.style.height = `${diameter}px`
      circle.style.left = `${
        event.clientX - clickedElement.offsetLeft - radius
      }px`
      circle.style.top = `${
        event.clientY - clickedElement.offsetTop - radius
      }px`
      circle.classList.add("ripple")
      const ripple = clickedElement.getElementsByClassName("ripple")[0]

      if (ripple) {
        ripple.remove()
      }
      clickedElement.appendChild(circle)
    }
    resolve(true)
  })

const withRipple = (callback, event) => {
  initiateRipple(event).then(() => {
    let timeout = setTimeout(() => {
      callback()
      clearTimeout(timeout)
    }, rippleTimeout)
  })
}

export default withRipple
