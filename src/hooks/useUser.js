import {UserContext} from "contexts/UserContexts"
import {useContext} from "react"

const useUser = () => useContext(UserContext)
export default useUser
