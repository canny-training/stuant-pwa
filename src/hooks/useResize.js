import {useState, useEffect} from "react"
import {MOBILE_WIDTH} from "constants/constants"

const useResize = () => {
  const [resized, onResize] = useState({
    isDesktop: true,
  })
  useEffect(() => {
    const handleResize = () =>
      onResize(prev => ({
        ...prev,
        isDesktop: window.innerWidth > MOBILE_WIDTH,
      }))

    window.addEventListener("resize", handleResize)
    handleResize()
    return () => window.removeEventListener("resize", handleResize)
  }, [])
  return resized
}

export default useResize
