import {useContext} from "react"
import BottomSheetContext from "contexts/BottomSheetContext"

const useBottomSheet = () => useContext(BottomSheetContext)
export default useBottomSheet
