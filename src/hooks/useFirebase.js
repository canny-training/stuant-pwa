import firebase from "firebase/app"
import "firebase/messaging"

const {
  // REACT_APP_FIREBASE_VAPID_KEY,
  REACT_APP_FIREBASE_API_KEY,
  REACT_APP_FIREBASE_AUTH_DOMAIN,
  REACT_APP_FIREBASE_DATABASE_URL,
  REACT_APP_FIREBASE_PROJECT_ID,
  REACT_APP_FIREBASE_STORAGE_BUCKET,
  REACT_APP_FIREBASE_MESSAGING_SENDER_ID,
  REACT_APP_FIREBASE_APP_ID,
  REACT_APP_FIREBASE_MEASUREMENT_ID,
} = process.env

const firebaseConfig = {
  apiKey: REACT_APP_FIREBASE_API_KEY,
  authDomain: REACT_APP_FIREBASE_AUTH_DOMAIN,
  databaseURL: REACT_APP_FIREBASE_DATABASE_URL,
  projectId: REACT_APP_FIREBASE_PROJECT_ID,
  storageBucket: REACT_APP_FIREBASE_STORAGE_BUCKET,
  messagingSenderId: REACT_APP_FIREBASE_MESSAGING_SENDER_ID,
  appId: REACT_APP_FIREBASE_APP_ID,
  measurementId: REACT_APP_FIREBASE_MEASUREMENT_ID,
}

if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig)
} else {
  firebase.app()
}

export const messaging = firebase.messaging.isSupported()
  ? firebase.messaging()
  : null

const useFirebase = () => {
  const getToken = async () => {
    const {REACT_APP_ENV} = process.env
    if (REACT_APP_ENV === "performance") {
      // doing it for preprod env only
      return "No token received"
    } else {
      if (messaging) {
        try {
          let token = await messaging.getToken({
            vapidKey: process.env.REACT_APP_VAPID_KEY,
          })
          return token
        } catch (error) {
          return "No token received"
        }
      } else {
        return "No token received"
      }
    }
  }
  return {
    getToken,
  }
}
export default useFirebase
