import {useContext} from "react"
import SnackContext from "contexts/SnackContext"

const useSnack = () => {
  return useContext(SnackContext)
}
export default useSnack
