import axios from "axios"
import useUser from "./useUser"

const useAxios = () => {
  const {userData} = useUser()
  const {access_token} = userData
  const {REACT_APP_API_BASE_URL} = process.env
  const instance = axios.create({
    baseURL: REACT_APP_API_BASE_URL,
  })
  instance.defaults.headers.common["Content-Type"] = "application/json"
  if (access_token)
    instance.defaults.headers.common["Authorization"] = `Bearer ${access_token}`
  if (instance) {
    instance.interceptors.request.use(
      config => {
        // TODO Do something with API Request
        return config
      },
      error => {
        return Promise.reject(error)
      },
    )
    instance.interceptors.response.use(
      res => res,
      async err => {
        // TODO Do something with API Response
        return Promise.reject(err)
      },
    )
  }
  return instance
}
export default useAxios
