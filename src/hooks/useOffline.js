import {useContext} from "react"
import OfflineContext from "contexts/OfflineContext"

const useOffline = () => useContext(OfflineContext)
export default useOffline
