const useInjectScript = () => {
  const injectScript = (src, defer = false, isHead = false) => {
    const script = document.createElement("script")
    script.src = src
    if (defer) script.defer = defer
    return new Promise((resolve, reject) => {
      if (isHead) {
        document.head.appendChild(script)
      } else {
        document.body.appendChild(script)
      }
      script.onload = () => {
        return resolve("new")
      }
      script.onerror = () => {
        return reject("error")
      }
    })
  }
  return {injectScript}
}

export default useInjectScript
