import {useContext} from "react"
import FooterContext from "contexts/FooterContext"
const useFooter = () => useContext(FooterContext)
export default useFooter
