import {lazy, useEffect, useState} from "react"
import {useHistory} from "react-router-dom"
import useBottomSheet from "hooks/useBottomSheet"
import useAuth from "hooks/useAuth"
import useModal from "hooks/useModal"
import useInjectScript from "hooks/useInjectScript"
import useResize from "hooks/useResize"
import Autocomplete from "components/GoogleAutocomplete"
import {deliveryLocation} from "pages/auth/storage-constants"
import {PAGE_ROUTE_AUTH_PHONE} from "routes/auth"
import {BASE_ROUTE, ROUTE_ACTIVE_ORDERS} from "routes/profile"
import {ORDER_TRACKING} from "routes/static"
import useAxios from "hooks/useAxios"
import {ACTIVE_ORDERS} from "pages/home/api-constants"
import {locationWidgetEvent} from "analytics/CannyAnalytics"
import {getPageURL, googleScriptSrc} from "constants/constants"

const DesktopHeader = lazy(() =>
  import(
    /* webpackChunkName: "desktop-header" */ "components/header/desktop-header"
  ),
)
const MobileHeader = lazy(() =>
  import(
    /* webpackChunkName: "mobile-header" */ "components/header/mobile-header"
  ),
)

const ResponsiveHeader = ({onSelectLocation, showMobileHeader = false}) => {
  const [deliveryLoc, setDeliveryLoc] = useState(
    JSON.parse(localStorage.getItem(deliveryLocation)),
  )
  const [activeOrders, setActiveOrders] = useState([])
  const {history} = useHistory()
  const {setSheetContent} = useBottomSheet()
  const {toggleModal} = useModal()
  const {authUser} = useAuth()
  const axios = useAxios()
  const {injectScript} = useInjectScript()
  const {isDesktop} = useResize()

  useEffect(() => {
    const session =
      localStorage.getItem("__QWEMNB__") &&
      JSON.parse(localStorage.getItem("__QWEMNB__"))
    if (session?.access_token && session?.customer_hash && authUser) {
      const {access_token, customer_hash} = session
      axios({
        url: ACTIVE_ORDERS,
        method: "get",
        headers: {
          customer_hash,
          access_token,
          is_guest_user: "no",
        },
      })
        .then(res => {
          if (res.data?.statusCode === 200 && res.data?.result?.length) {
            setActiveOrders(res.data?.result)
          } else {
            setActiveOrders([])
          }
        })
        .catch(err => {
          setActiveOrders([])
        })
    }

    const execute = () => {
      if (!deliveryLoc)
        if (!navigator.geolocation) {
          return
        } else {
          const google = window.google
          const geocoder = new google.maps.Geocoder()
          navigator.geolocation.getCurrentPosition(
            position => {
              const {latitude, longitude} = position.coords
              let latlng = new google.maps.LatLng(latitude, longitude)
              geocoder.geocode({latLng: latlng}, (results, status) => {
                if (status === google.maps.GeocoderStatus.OK) {
                  const locationObj = {}
                  if (results[0]) {
                    locationObj.address = results[0].formatted_address
                    locationObj.address_components =
                      results[0].address_components
                    locationObj.lat = latitude
                    locationObj.lng = longitude
                    geocoder
                      .geocode({
                        location: {
                          lat: locationObj.lat,
                          lng: locationObj.lng,
                        },
                      })
                      .then(response => {
                        if (response.results[0]) {
                          locationObj.address_components =
                            response.results[0]?.address_components
                          onChangeLocation(locationObj)
                        } else {
                          throw new Error("No address found")
                        }
                      })
                      .catch(e => {
                        console.log("Something went wrong", e) //eslint-disable-line
                      })
                  } else {
                    return
                  }
                } else {
                  return
                }
              })
              locationWidgetEvent()
            },
            error => {
              setSheetContent(
                [
                  <Autocomplete
                    setDeliveryLoc={onChangeLocation}
                    showSnack={true}
                  />,
                ],
                false,
                true,
              )
              return
            },
          )
        }
    }
    if (window.google) {
      execute()
    } else {
      injectScript(googleScriptSrc)
        .then(() => {
          execute()
        })
        .catch(e => {
          console.log("Something went wrong in injecting the google script", e) //eslint-disable-line
        })
    }
  }, []) //eslint-disable-line

  const onChangeLocation = locationObj => {
    setDeliveryLoc(locationObj)
    localStorage.setItem(deliveryLocation, JSON.stringify(locationObj))
    onSelectLocation(locationObj)
  }

  const handleTrackerClick = () =>
    activeOrders.length >= 1
      ? history.push(ROUTE_ACTIVE_ORDERS, activeOrders)
      : history.push({
          pathname: ORDER_TRACKING,
          state: {
            order_id: activeOrders[0].order_id,
          },
        })

  const onClickLocation = () =>
    setSheetContent(
      [<Autocomplete setDeliveryLoc={onChangeLocation} />],
      true,
      true,
    )

  const routeToPhonePage = () =>
    history.push(PAGE_ROUTE_AUTH_PHONE, getPageURL())
  const routeToProfilePage = () => {
    const session =
      localStorage.getItem("__QWEMNB__") &&
      JSON.parse(localStorage.getItem("__QWEMNB__"))
    if (!authUser || !session) {
      toggleModal({
        message: "Please login to access your profile",
        secondaryButtonText: "Login",
        primaryButtonText: "Cancel",
        onSecondaryClick: routeToPhonePage,
      })
      return
    }
    history.push(BASE_ROUTE)
  }

  const onClickOffer = () => console.log("Clicked offers")

  const onClickCart = () => console.log("Clicked Carts")

  const onClickSupport = () => console.log("Clicked Supports")

  return isDesktop ? (
    <DesktopHeader
      showTrackIcon={activeOrders.length > 0}
      deliveryLoc={deliveryLoc}
      authUser={authUser}
      onClickOffer={onClickOffer}
      onClickCart={onClickCart}
      onClickSupport={onClickSupport}
      onClickProfile={routeToProfilePage}
      onClickTrack={handleTrackerClick}
      onClickLocation={onClickLocation}
    />
  ) : showMobileHeader ? (
    <MobileHeader
      showTrackIcon={activeOrders.length > 0}
      deliveryLoc={deliveryLoc}
      authUser={authUser}
      onClickProfile={routeToProfilePage}
      onClickTrack={handleTrackerClick}
      onClickLocation={onClickLocation}
    />
  ) : null
}

export default ResponsiveHeader
