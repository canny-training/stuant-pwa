import styled from "styled-components/macro"
export const Left = styled.div`
  height: 100%;
  display: flex;
  justify-content: flex-start;
  align-items: center;
  padding-left: 1em;
  .svg-wrapper {
    display: flex;
    padding-right: 0.64em;
  }
  .header-title {
    font-size: 1em;
    @media (max-width: 350px) {
      font-size: 0.9em;
    }
    color: var(--hex-eight);
    max-width: 100%;
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
  }
  .inverted-header-title {
    color: var(--hex-one);
  }
  path {
    fill: ${props => (props.backInvert ? "white" : "")};
  }
`
export const AppLogoWrapper = styled.span`
  position: absolute;
  top: 0.7em;
  right: 1.1em;
  display: flex;
  svg {
    width: 1.228em;
    height: 1.228em;
  }
`
