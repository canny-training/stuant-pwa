import {useContext, useEffect, memo, Fragment} from "react"
import ReactDOM from "react-dom"
import {Left} from "./style"
import ic_left from "assets/icons/ic_left.svg"
import HeaderContext from "contexts/HeaderContext"

const headerRoot = document.getElementById("header-root")
const Header = () => {
  let headerChild
  const {header, content} = useContext(HeaderContext)
  const {
    text,
    textInvert = false,
    showChevron = true,
    backInvert = false,
    transparent,
    custom = false,
    fixed = true,
    boxShadow = true,
  } = content

  useEffect(() => {
    const inset = fixed ? 0 : `unset`
    headerRoot.style.visibility = header ? `visible` : `hidden`
    headerRoot.style.position = fixed ? `fixed` : `unset`
    headerRoot.style.top = inset
    headerRoot.style.right = inset
    headerRoot.style.bottom = inset
    headerRoot.style.left = inset
    headerRoot.style.fontSize = `1.375rem`
    headerRoot.style.height = custom ? `max-content` : header ? `2.55em` : `0`
    headerRoot.style.zIndex = fixed ? 1 : `unset`
    headerRoot.style.backgroundColor = transparent
      ? `transparent`
      : `var(--hex-one)`
    headerRoot.style.boxShadow = !boxShadow
      ? "none"
      : `0px 0px 8px rgba(var(--rgb-nine), var(--alpha-two))`
  }, [header, custom, fixed, transparent, boxShadow])

  const renderHeaderStyle = `bold header-title ${
    textInvert && "inverted-header-title"
  }`
  headerChild = custom || (
    <Fragment>
      <Left backInvert={backInvert}>
        {showChevron && <img src={ic_left} alt="" />}
        <span className={renderHeaderStyle}>{text}</span>
      </Left>
    </Fragment>
  )
  if (!header) {
    return null
  }
  return ReactDOM.createPortal(headerChild, headerRoot)
}
export default memo(Header)
