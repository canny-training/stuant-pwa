import styled, {keyframes} from "styled-components"

const skeletonLoading = keyframes`
  0%{
    background-color: hsl(200,20%,70%);
  }
  100%{
    background-color: hsl(200,20%,95%);
  }
`

export const Container = styled.div`
  background-color: var(--hex-one);
  position: fixed;
  z-index: 1;
  width: 100%;
  box-shadow: 0px 0px 2px rgba(var(--rgb-ten), var(--alpha-two));

  h4,
  span {
    margin: 0;
  }

  .sub-locality-skeleton,
  .locality-skeleton {
    width: 8rem;
    height: 0.8rem;
    border-radius: 4px;
    animation: ${skeletonLoading} 1s linear infinite alternate;
  }

  .sub-locality-skeleton {
    margin-bottom: 0.4rem;
    width: 10rem;
  }

  .wrapper {
    padding: 0.5rem 1.5rem;
  }
  .wrapper,
  .left {
    display: flex;
    justify-content: space-between;
    align-items: center;
  }

  .address {
    display: flex;
    flex-direction: column;
    margin-left: 0.5rem;
  }

  .locality,
  .sub-locality {
    text-overflow: ellipsis;
    overflow: hidden;
    text-align: left;
    white-space: nowrap;
    line-height: 1rem;
    width: 10rem;
  }

  .locality {
    font-size: 0.625rem;
    margin-top: 0.25rem;
  }

  .sub-locality {
    font-size: 0.75rem;
  }

  .right {
    display: flex;
    justify-content: flex-end;
    align-items: center;
  }

  .profile {
    font-size: 0.75rem;
    color: var(--hex-seven);
    width: 1.875rem;
    height: 1.875rem;
    border-radius: 50%;
    margin-left: 1rem;
    border: 1.4px solid var(--hex-seven);
    padding: 0;
    background: transparent;

    &.copper {
      background: linear-gradient(
        145.38deg,
        rgba(204, 151, 96, 0.5) -0.39%,
        rgba(239, 182, 95, 0.5) 101.53%
      );
      color: var(--hex-twenty-nine);
      border: 1.4px solid var(--hex-twenty-nine);
    }

    &.silver {
      background: linear-gradient(
        145.38deg,
        rgba(124, 119, 119, 0.5) -0.39%,
        rgba(181, 172, 172, 0.5) 101.53%
      );
      color: var(--hex-thirty);
      border: 1.4px solid var(--hex-thirty);
    }

    &.gold {
      background: linear-gradient(
        145.38deg,
        rgba(141, 114, 73, 0.5) -0.39%,
        rgba(181, 160, 128, 0.5) 101.53%
      );
      color: var(--hex-thirty-one);
      border: 1.4px solid var(--hex-thirty-one);
    }

    &.platinum {
      background: linear-gradient(
        145.38deg,
        rgba(22, 22, 29, 0.5) -0.39%,
        rgba(110, 110, 115, 0.5) 101.53%
      );
      color: var(--hex-thirty-two);
      border: 1.4px solid var(--hex-thirty-two);
    }
  }
`
