import {Container} from "./styles"
import ic_location from "assets/icons/location.svg"
import ic_guest from "assets/icons/guest-user.svg"
import ic_clock from "assets/icons/clock.svg"

const MobileHeader = ({
  deliveryLoc,
  authUser,
  showTrackIcon,
  onClickProfile,
  onClickLocation,
  onClickTrack,
}) => {
  const {address_components} = deliveryLoc ?? {}
  const locality = address_components?.find(it =>
    it.types.includes("locality"),
  )?.short_name
  const subLocality =
    address_components?.find(it => it.types.includes("sublocality_level_1"))
      ?.short_name ?? locality
  const {tier, fname, lname} = authUser ?? {}

  return (
    <Container>
      <div className="wrapper">
        <div className="left" onClick={onClickLocation}>
          <div className="line"></div>
          <img className="location" src={ic_location} alt="" />
          {deliveryLoc ? (
            <div className="address">
              {locality !== subLocality && (
                <h4 className="sub-locality">{subLocality}</h4>
              )}
              <span className="locality light">{locality}</span>
            </div>
          ) : (
            <div>
              <div className="sub-locality-skeleton"></div>
              <div className="locality-skeleton"></div>
            </div>
          )}
        </div>
        <div className="right">
          {showTrackIcon && (
            <img src={ic_clock} alt="" onClick={onClickTrack} />
          )}
          <div className="line"></div>
          <div onClick={onClickProfile} className="chevron-button">
            {!authUser || !fname || !lname ? (
              <img src={ic_guest} alt="" onClick={onClickProfile} />
            ) : (
              <button className={`bold profile ${tier?.toLowerCase()}`}>
                {fname?.[0]?.toUpperCase() + lname?.[0]?.toUpperCase()}
              </button>
            )}
          </div>
        </div>
      </div>
    </Container>
  )
}

export default MobileHeader
