import styled, {keyframes} from "styled-components"

const skeletonLoading = keyframes`
  0%{
    background-color: hsl(200,20%,70%);
  }
  100%{
    background-color: hsl(200,20%,95%);
  }
`

export const Container = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  position: fixed;
  z-index: 1;
  width: 100%;
  background-color: rgba(var(--rgb-one), var(--alpha-nine));

  h4,
  span {
    margin: 0;
  }

  .sub-locality-skeleton,
  .locality-skeleton {
    width: 8rem;
    height: 0.8rem;
    border-radius: 4px;
    animation: ${skeletonLoading} 1s linear infinite alternate;
  }

  .sub-locality-skeleton {
    margin-bottom: 0.4rem;
    width: 10rem;
  }

  .wrapper,
  .left {
    display: flex;
    justify-content: space-between;
    align-items: center;
  }

  .address {
    display: flex;
    flex-direction: column;
    margin-left: 0.5rem;
  }

  .locality,
  .sub-locality {
    text-overflow: ellipsis;
    overflow: hidden;
    text-align: left;
    white-space: nowrap;
    line-height: 1.25rem;
    width: 10rem;
  }

  .locality {
    font-size: 1rem;
    margin-top: 0.25rem;
  }

  .sub-locality {
    font-size: 1.125rem;
  }

  .right {
    display: flex;
    justify-content: flex-end;
    align-items: center;
  }

  .location {
    margin-left: 1.5rem;
  }

  .line {
    width: 1px;
    height: 4.5rem;
    background-image: linear-gradient(
      rgba(var(--rgb-nine), var(--alpha-zero)),
      var(--hex-nine),
      rgba(var(--rgb-nine), var(--alpha-zero))
    );
  }

  .profile {
    width: 1.25rem;
    height: 1.25rem;
    font-size: 0.5rem;
    margin-bottom: 0.25rem;
    color: var(--hex-seven);
    border-radius: 50%;
    border: 1.4px solid var(--hex-seven);
    padding: 0;
    background: transparent;

    &.copper {
      background: linear-gradient(
        145.38deg,
        rgba(204, 151, 96, 0.5) -0.39%,
        rgba(239, 182, 95, 0.5) 101.53%
      );
      color: var(--hex-twenty-nine);
      border: 1.4px solid var(--hex-twenty-nine);
    }

    &.silver {
      background: linear-gradient(
        145.38deg,
        rgba(124, 119, 119, 0.5) -0.39%,
        rgba(181, 172, 172, 0.5) 101.53%
      );
      color: var(--hex-thirty);
      border: 1.4px solid var(--hex-thirty);
    }

    &.gold {
      background: linear-gradient(
        145.38deg,
        rgba(141, 114, 73, 0.5) -0.39%,
        rgba(181, 160, 128, 0.5) 101.53%
      );
      color: var(--hex-thirty-one);
      border: 1.4px solid var(--hex-thirty-one);
    }

    &.platinum {
      background: linear-gradient(
        145.38deg,
        rgba(22, 22, 29, 0.5) -0.39%,
        rgba(110, 110, 115, 0.5) 101.53%
      );
      color: var(--hex-thirty-two);
      border: 1.4px solid var(--hex-thirty-two);
    }
  }

  .logo {
    margin: 1.5rem 2rem 0.75rem 0;
    width: 5.6rem;
    height: 3.25rem;
  }
`
export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
  max-width: 6rem;
  height: 100%;
  padding: 0 1.5rem;

  span {
    color: var(--hex-nine);
    text-overflow: ellipsis;
    overflow: hidden;
    white-space: nowrap;
    font-size: 1rem;
    margin: 0;
  }
`
