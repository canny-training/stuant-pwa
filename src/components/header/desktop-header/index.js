import {Container, Wrapper} from "./styles"
import ic_location from "assets/icons/location.svg"
import ic_guest from "assets/icons/guest-user.svg"
import ic_clock from "assets/icons/clock.svg"
import ic_logo from "assets/icons/ic-qmin-cover.svg"
import ic_cart from "assets/icons/ic-cart.svg"
import ic_support from "assets/icons/ic-support.svg"
import ic_tag from "assets/icons/ic-tag.svg"

const DesktopHeader = ({
  deliveryLoc,
  authUser,
  showTrackIcon,
  onClickOffer,
  onClickProfile,
  onClickCart,
  onClickSupport,
  onClickLocation,
  onClickTrack,
}) => {
  const {address_components} = deliveryLoc ?? {}
  const locality = address_components?.find(it =>
    it.types.includes("locality"),
  )?.short_name
  const subLocality =
    address_components?.find(it => it.types.includes("sublocality_level_1"))
      ?.short_name ?? locality
  const {tier, fname, lname} = authUser ?? {}
  return (
    <Container>
      <div className="dt-container wrapper">
        <div className="left" onClick={onClickLocation}>
          <img className="logo" src={ic_logo} alt="" />
          <div className="line"></div>
          <img className="location" src={ic_location} alt="" />
          {deliveryLoc ? (
            <div className="address">
              {locality !== subLocality && (
                <h4 className="sub-locality">{subLocality}</h4>
              )}
              <span className="locality light">{locality}</span>
            </div>
          ) : (
            <div>
              <div className="sub-locality-skeleton"></div>
              <div className="locality-skeleton"></div>
            </div>
          )}
        </div>
        <div className="right">
          {showTrackIcon && (
            <img src={ic_clock} alt="" onClick={onClickTrack} />
          )}
          <HeaderItem icon={ic_tag} text="Offers" onClick={onClickOffer} />
          <HeaderItem icon={ic_cart} text="Cart" onClick={onClickCart} />
          <HeaderItem
            icon={ic_support}
            text="Support"
            onClick={onClickSupport}
          />
          <div className="line"></div>
          <div onClick={onClickProfile} className="chevron-button">
            {!authUser || !fname || !lname ? (
              <img src={ic_guest} alt="" />
            ) : (
              <Wrapper onClick={onClickProfile}>
                <button className={`bold profile ${tier?.toLowerCase()}`}>
                  {fname?.[0]?.toUpperCase() + lname?.[0]?.toUpperCase()}
                </button>
                <span>Hello {fname}</span>
              </Wrapper>
            )}
          </div>
        </div>
      </div>
    </Container>
  )
}

export default DesktopHeader

const HeaderItem = ({icon, text, onClick}) => {
  return (
    <Wrapper onClick={onClick}>
      <img src={icon} alt="" />
      <span>{text}</span>
    </Wrapper>
  )
}
