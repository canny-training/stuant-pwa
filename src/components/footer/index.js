import {useContext, useEffect, memo} from "react"
import ReactDOM from "react-dom"
import FooterContext from "contexts/FooterContext"
const footerRoot = document.getElementById("footer-root")

const Footer = () => {
  const {footer, content} = useContext(FooterContext)
  const {custom = false, fixed = true} = content

  useEffect(() => {
    const inset = fixed ? 0 : `unset`
    footerRoot.style.position = fixed ? `fixed` : `unset`
    footerRoot.style.right = inset
    footerRoot.style.bottom = inset
    footerRoot.style.left = inset
    footerRoot.style.fontSize = `1rem`
    footerRoot.style.height = custom ? `max-content` : `0`
    footerRoot.style.zIndex = fixed ? 1 : `unset`
    footerRoot.style.backgroundColor = `var(--hex-white)`
  }) //eslint-disable-line
  return footer ? ReactDOM.createPortal(custom, footerRoot) : null
}
export default memo(Footer)
