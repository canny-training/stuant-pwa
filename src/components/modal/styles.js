import styled from "styled-components/macro"

export const ModalContainer = styled.div`
  position: fixed;
  height: 100%;
  width: 100%;
  top: 0;
  left: 0;
  display: flex;
  align-items: center;
  justify-content: center;
  animation: transitionoverlay 0.3s ease-in-out;
  background-color: rgba(var(--rgb-ten), var(--alpha-five));
  backdrop-filter: blur(3px);

  @keyframes transitionoverlay {
    from {
      background-color: rgba(var(--rgb-ten), var(--alpha-zero));
    }
    to {
      background-color: rgba(var(--rgb-ten), var(--alpha-five));
    }
  }
`

export const ModalContent = styled.div`
  background-color: var(--hex-one);
  border-radius: 8px;
  position: absolute;
  left: 32px;
  right: 32px;
  height: 14em;
  display: grid;
  justify-content: stretch;
  align-items: center;
  animation: opacityFrame 0.2s ease-in-out;
  opacity: 1;

  @keyframes opacityFrame {
    from {
      opacity: 0;
    }
    to {
      opacity: 1;
    }
  }

  img {
    margin: 0 auto;
  }

  .content-text {
    overflow-x: hidden;
    width: 82%;
    text-align: center;
    margin: 0 auto;
  }

  .button-group {
    display: flex;
    justify-content: space-evenly;
    align-items: center;
  }

  .button-text {
    background-color: transparent;
    font-size: 14px;
    line-height: 20px;
    display: flex;
    align-items: center;
    color: var(--hex-five-secondary);
  }
`
