import {useContext, useEffect} from "react"
import ReactDOM from "react-dom"
import ModalContext from "contexts/ModalContext"
import {ModalContainer, ModalContent} from "./styles"

// These two containers are siblings in the DOM
const modalRoot = document.getElementById("modal-root")
const el = document.createElement("div")

const Modal = () => {
  const {modal, content, toggleModal} = useContext(ModalContext)
  const {
    message,
    primaryButtonText,
    onPrimaryClick,
    secondaryButtonText,
    onSecondaryClick,
    custom,
    closable = true,
  } = content

  useEffect(() => {
    modalRoot.appendChild(el)

    return () => {
      toggleModal()
      modalRoot.removeChild(el)
    }
  }, []) // eslint-disable-line

  useEffect(() => {
    if (modal) {
      document.body.style.overflowY = "hidden"
    } else {
      document.body.style.removeProperty("overflow-y")
    }
  }, [modal])
  const closeModal = e =>
    e.target.id === "modal-container" && closable && toggleModal()
  const handlePrimaryClick = () => {
    toggleModal()
    onPrimaryClick && onPrimaryClick()
  }
  const handleSecondaryClick = () => {
    toggleModal()
    onSecondaryClick && onSecondaryClick()
  }

  return modal
    ? ReactDOM.createPortal(
        <ModalContainer
          id="modal-container"
          role="dialog"
          onClick={closeModal}
          aria-describedby="modalDescription"
        >
          <ModalContent>
            <div className="content-text" id="modalDescription">
              {custom || message}
            </div>
            <div className="button-group">
              {primaryButtonText && (
                <button
                  className="button-text primary-btn"
                  onClick={handlePrimaryClick}
                >
                  {primaryButtonText}
                </button>
              )}
              {secondaryButtonText && (
                <button
                  className="button-text secondary-btn"
                  onClick={handleSecondaryClick}
                >
                  {secondaryButtonText}
                </button>
              )}
            </div>
          </ModalContent>
        </ModalContainer>,
        el,
      )
    : null
}
export default Modal
