import styled from "styled-components/macro"

export const Container = styled.div`
  display: flex;
  width: 100vw;
  height: 100vh;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  position: absolute;
  z-index: 100;

  .bg-image {
    position: absolute;
    width: 100vw;
    height: 100vh;
    z-index: -1;
  }
  .text,
  .sub-text-1,
  .sub-text-2 {
    font-size: 1.5rem;
    line-height: 1em;
    color: var(--hex-one);
  }
  .text {
    margin-top: 0.5em;
    margin-bottom: 0.25em;
  }
`
