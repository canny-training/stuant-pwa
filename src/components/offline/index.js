import ReactDOM from "react-dom"
import useOffline from "hooks/useOffline"
import {Container} from "./styles"

const Offline = () => {
  const {offline} = useOffline()
  if (!offline) return null
  return ReactDOM.createPortal(
    <Container>
      <div className="text bold">Something new is cooking</div>
      <div className="sub-text-1 bold">Kindly call 18002667646</div>
      <div className="sub-text-2 bold">to place your order</div>
    </Container>,
    document.querySelector("#offline-root"),
  )
}

export default Offline
