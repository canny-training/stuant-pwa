import styled from "styled-components"
import {ReactComponent as ArrowDown} from "assets/icons/arrow_down.svg"
const timing = "0.5s all ease-in-out"

export const FormLabelStyled = styled.label`
  position: absolute;
  top: ${props => (props.focus ? "auto" : "0.25em")};
  transform: ${props => (props.focus ? "translateY(-1em)" : "none")};
  color: {props => (props.error ? var(--hex-seven) : props.color || var(--hex-eight))}; 
  font-size: ${props => (props.focus ? "0.8em" : "1em")};
  line-height: 1.43;
  height: 1.5em;
  transition: 0.2s all ease-in-out;
`

export const HintStyled = styled.span`
  font-size: 0.75em;
  color: var(--hex-six);
`

export const ErrorStyled = styled.span`
  font-size: 0.75em;
  line-height: 1em;
  color: {props => props.color || var(--hex-seven)};
`

export const DropDownIconStyled = styled(ArrowDown)`
  position: absolute;
  right: 0;
  top: -0.25em;
`

export const ErrorIconStyled = styled(ArrowDown)`
  position: absolute;
  right: 0;
  top: 0.5em;
`
export const IconWrapper = styled.span`
  position: relative;
  top: -0.25em;
  margin-left: 0.5em;
`

export const InputFormGroupStyled = styled.div`
  margin-bottom: 1.5em;
  margin-top: 1.25em;
  position: relative;

  & input {
    color: {props => (props.disabled ? var(--hex-eight) : var(--hex-one))};
    display: block;
    height: 2em;
    background: transparent;
    border: none;
    font-size: 1em;
    line-height: 1.5;
    width: 100%;
    padding: 0.2em 0 0.1em;
    transition: 0.2s all ease-in-out;
    cursor: ${props => (props.disabled ? "not-allowed" : "auto")};
  }

  & input:focus {
    outline: none;
  }

  & .error {
    font-size: 0.5em;
    color: {props => props.color || var(--hex-six)};
  }
`

export const SelectFormGroupStyled = styled.div`
  margin-bottom: 1.5em;
  margin-top: 1.2em;
  position: relative;

  & select {
    color: {props => (props.disabled ? var(--hex-eight) : var(--hex-one))};
    background: transparent;
    font-size: 1.2em;
    line-height: 1.5em;
    display: block;
    height: 2em;
    padding-bottom: .25em;
    background-clip: padding-box;
    border-right: none;
    border-top: none;
    border-left: none;
    border-bottom: 0.1em solid {props => (props.error ? var(--hex-seven) : var(--hex-eight))};
    border-radius: 0;
    word-wrap: normal;
    text-align: left;
    min-width: 7.8em;
    width: 100%;
    transition: 0.2s all ease-in-out;
    appearance: none;
    cursor: ${props => (props.disabled ? "not-allowed" : "auto")};
  }

  & select:focus {
    outline: none;
  }
`

export const SelectLabel = styled.div`
  color: {({color}) => color || var(--hex-eight)};
  margin: ${props => (props.margin ? props.margin : "0 0 0.25em 0")};
  font-size: {props => (props.fontSize ? props.fontSize : var(--hex-six))};
  ${props => props.disabled && `color:var(--hex-six);`};
  line-height: 1.43;
  text-align : center;
  height: 1.75em;
  & span {
    font-size: 1rem;
      line-height: 1.5em;
      letter-spacing: 1px;
     
    text-transform: uppercase;
    color: inherit
  }
`

export const SelectButton = styled.button`
  text-align: left;
  color: inherit;
  background-color: transparent;
  border: unset;
  width: 100%;
  position: relative;
  padding: 0.1em 0em;
  border-bottom: 2px solid rgba(var(--rgb-nine), 0.16);
  font-size: ${props => (props.fontSize ? props.fontSize : "1em")};
`

export const SelectValue = styled.div`
  margin: ${props => (props.margin ? props.margin : "0 0 0.25em")};
  text-align: ${props => (props.textAlign === "left" ? "left" : "center")};
  letter-spacing: 0.4px;

  color: inherit;
  .selected-value {
    letter-spacing: 2px;
    color: rgba(var(--rgb-thir), var(--alpha-five));
  }
`

export const UL = styled.ul`
  position: absolute;
  width: 100%;
  background-color: var(--hex-one);
  color: {({color}) => color || var(--hex-two)};
  margin: 0;
  padding: 0;
  text-align: left;
  border: ${`0.1em solid {var(--hex-eight)}`};
  font-size: ${props => (props.fontSize ? props.fontSize : "1em")};
  z-index: 2;
  overflow-y: scroll;
  &::-webkit-scrollbar {
    width: 0.25em;
  }
  &::-webkit-scrollbar-track {
    background-color: rgba(0, 0, 0, 0);
  }
  &::-webkit-scrollbar-thumb {
    background-color: #8f8f8f;
    border-radius: 0.25em;
  }
  & li {
    padding: 0.75em 0.5em;
    display: block;
    text-align: ${props => (props.textAlign === "left" ? "left" : "center")};
    color: var(--hex-two);
    border:1px solid var(--hex-twe);
    border-bottom:none;
  }
  & li:last-child {
    border:1px solid var(--hex-twe);
  }
  & li:not([disabled]) {
    &:focus {
      background-color: ${({highlightBackgroundColor}) =>
        highlightBackgroundColor || "#F6F4EA"};
      ${({highlightColor}) =>
        highlightColor ? `color: ${highlightColor}` : ""}
      outline: none;
    }
    &:hover {
      cursor: default;
      background-color: ${({highlightBackgroundColor}) =>
        highlightBackgroundColor || "#F6F4EA"};
      ${({highlightColor}) =>
        highlightColor ? `color: ${highlightColor}` : ""}
    }
  }
  .disabled {
    color: var(--hex-six);
    cursor: not-allowed;
  }
  &:focus {
    outline: none;
  }
  
`

export const ChevronBottomSVG = styled(DropDownIconStyled)`
  bottom: 0.25em;
  top: unset;
  right: ${props => (props.right ? props.right : "0.5em")};
  ${props => props.rotate && `transform:rotate(180deg)`};
  will-change: transform;
  transition: ${`${timing}`};
  width: ${props => (props.width ? props.width : "1.5em")};
  height: ${props => (props.height ? props.height : "1em")};
  @media (min-width: 992px) {
    width: ${props => (props.width ? props.width : "1.5em")};
    height: ${props => (props.height ? props.height : "1.5em")};
  }
`

export const StyledFlex = styled.div`
  border-bottom: ${({error, readOnly}) =>
    readOnly
      ? "none"
      : `0.1em solid {error ? var(--hex-seven) : var(--hex-eight)}`};
  ${({readOnly}) => readOnly && "padding-top: 1em;"};
`
