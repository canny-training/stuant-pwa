import {useState, useEffect, Fragment, useRef} from "react"
import PropTypes from "prop-types"
import {
  SelectButton,
  UL,
  ChevronBottomSVG,
  SelectValue,
  ErrorStyled,
} from "./style"

let id = 0
/**
 * DropDown component
 * @param {Object} props - props object
 * @param {string} props.label - label for custom select
 * @param {Object} props.headingInputProps - props to pass on label div
 * @param {Object} props.headingInputProps - props to be passed on select button
 * @param {Object} props.ulInputProps - props to be passed on ul
 * @param {boolean} props.toggleState - props which detrmine the state of custom select expand/collapse
 * @param {Object} props.iconInputProps - props to be passed on icon button
 * @param {function} props.onChange - callback function onchange
 * @param {Array} props.options - li/list element
 * @param {string} props.value - set the default selected value in selectbox
 * @param {string} props.errorMsg - if set display the errorMsg
 * @param {boolean} props.closeDropDown - pass to close the custom select, it is used for internal purpose dont use it if you wish to toggle state then use toggleState
 * @param {number} props.elementNumber - identify the child order, don't use it
 * @param {function} props.closeOtherCallBack - callback from DropDownGroup, don't use it
 * @param {boolean} props.required - shows whether the field is required or not marked by asterisk
 */
const DropDown = props => {
  const {
    label,
    headingInputProps,
    buttonInputProps,
    ulInputProps,
    toggleState,
    iconInputProps,
    onChange,
    options,
    value,
    errorMsg,
    closeDropDown,
    elementNumber,
    closeOtherCallBack,
    textAlign,
    margin,
    selectStyle,
  } = props
  const [toggleSelect, handleToggleSelect] = useState(toggleState)
  const [selectedValue, setSelectedValue] = useState(value || label)
  const [search, setSearch] = useState("")
  const ref = useRef()
  const isDisabled = buttonInputProps?.disabled ?? false
  const refParent = useRef()
  const containerRef = useRef()
  refParent.current = elementNumber
  useEffect(() => {
    id++
    return () => {
      id = 0
    }
  }, [])

  useEffect(() => {
    handleToggleSelect(toggleSelect)
  }, [toggleSelect])

  useEffect(() => {
    if (closeDropDown) {
      handleToggleSelect(false)
    }
  }, [closeDropDown])

  const getUlElement = element => {
    if (element.type === "button") {
      return element.nextElementSibling
    }
    const parentElement = element.parentElement
    return getUlElement(parentElement)
  }

  const getFocusableLI = element => {
    const findLi = LIElement => {
      if (selectedValue === "Select" && LIElement && LIElement.tabIndex === 0) {
        return LIElement
      } else if (
        selectedValue &&
        LIElement &&
        LIElement.textContent === selectedValue
      ) {
        return LIElement
      } else if (!LIElement) {
        return null
      }
      return findLi(LIElement.nextElementSibling)
    }
    return findLi(element)
  }

  const handleToggleState = e => {
    if (typeof onChange === "function") {
      onChange(e)
    }
    handleToggleSelect(prevState => !prevState)
    // setting focus to the first li element
    if (!e) return
    const currentUL = getUlElement(e.target)
    if (!toggleSelect) {
      setTimeout(() => {
        const firstFocusableLI = getFocusableLI(
          currentUL && currentUL.firstElementChild,
        )
        firstFocusableLI && firstFocusableLI.focus()
      }, 200)
    }
    if (typeof closeOtherCallBack === "function") {
      closeOtherCallBack(e, refParent)
    }
  }

  const handleClick = e => {
    e.stopPropagation()
    const val = e.target.innerHTML
    if (e.target.tabIndex !== -1 && e.target.tagName === "LI") {
      if (typeof onChange === "function") {
        onChange(e, val)
      }
      handleToggleSelect(prevState => !prevState)
      setSelectedValue(val)
    }
  }

  const filterOption = e => {
    e.stopPropagation()
    const {charCode} = e
    if (charCode === 13) {
      handleClick(e)
      return 0
    }
    //very basic implementation for searched element
    setTimeout(() => {
      setSearch("")
    }, 1500)

    if (
      (charCode >= 97 && charCode <= 122) ||
      (charCode >= 65 && charCode <= 90)
    ) {
      const key = e.key
      setSearch(prevState => prevState + key)
    } else {
      setSearch("")
    }
  }

  useEffect(() => {
    const currentUL = ref.current
    if (search && search.length > 0) {
      const LiElements = Array.from(currentUL.children)
      LiElements.some(item => {
        if (
          item.innerText
            .toLowerCase()
            .slice(0, search.length)
            .indexOf(search.toLowerCase()) > -1 &&
          item.tabIndex !== -1
        ) {
          item.focus()
          return true
        }
        return false
      })
    }
  }, [search])

  const optionList = options.map((item, index) => {
    const nonFocuable = item?.inputProps?.disabled ?? false
    const incomingClasses = item.inputProps?.className ?? ""
    const classes = nonFocuable
      ? `${incomingClasses} disabled`
      : incomingClasses

    return (
      <li
        role="option"
        aria-label={`select ${item.label}`}
        key={`select-list-${id}-${index}`}
        tabIndex={nonFocuable ? "-1" : "0"}
        {...item.inputProps}
        className={
          selectedValue === item.label ? `selected-li ${classes}` : classes
        }
        aria-selected={selectedValue === item.label}
      >
        {item.label}
      </li>
    )
  })

  useEffect(() => {
    const handleWindowClick = e => {
      if (!containerRef.current.contains(e.target)) {
        handleToggleState()
      }
    }
    if (toggleSelect) {
      window.addEventListener("click", handleWindowClick)
    }
    return () => {
      window.removeEventListener("click", handleWindowClick)
    }
    // eslint-disable-next-line
  }, [toggleSelect])

  return (
    <Fragment>
      {/* <SelectLabel
        id={`select-box-label ${label}`}
        {...headingInputProps}
        disabled={isDisabled ? 1 : 0}
      >
        <span>
          {label}
          {required ? "*" : ""}
        </span>
      </SelectLabel> */}
      <div ref={containerRef} style={{position: "relative"}}>
        <SelectButton
          id={`select-button ${label}`}
          aria-haspopup="listbox"
          aria-labelledby={headingInputProps?.id ?? "select-box-label"}
          type="button"
          value="select"
          {...buttonInputProps}
          onClick={handleToggleState}
          onKeyPress={filterOption}
          isError={!!errorMsg}
          disabled={isDisabled ? 1 : 0}
          style={selectStyle}
        >
          <SelectValue
            textAlign={textAlign}
            margin={margin}
            disabled={isDisabled ? 1 : 0}
          >
            {selectedValue === label ? (
              <span className="selected-value bold">{selectedValue}</span>
            ) : (
              <span
                className="selected-value bold"
                style={{color: "var(--hex-seventeen)", letterSpacing: "0.4px"}}
              >
                {selectedValue}
              </span>
            )}
          </SelectValue>
          <ChevronBottomSVG
            rotate={toggleSelect ? 1 : 0}
            fill={isDisabled ? "var(--hex-six)" : ""}
            {...iconInputProps}
          />
        </SelectButton>
        <UL
          role="listbox"
          tabIndex={toggleSelect ? 0 : -1}
          aria-hidden={!toggleSelect}
          {...ulInputProps}
          hidden={!toggleSelect}
          onClick={handleClick}
          onKeyPress={filterOption}
          ref={ref}
          textAlign={textAlign}
        >
          {optionList}
        </UL>
      </div>
      {errorMsg && <ErrorStyled>{errorMsg}</ErrorStyled>}
    </Fragment>
  )
}

DropDown.propTypes = {
  label: PropTypes.string,
  headingInputProps: PropTypes.object,
  buttonInputProps: PropTypes.object,
  ulInputProps: PropTypes.object,
  toggleState: PropTypes.bool,
  iconInputProps: PropTypes.object,
  onChange: PropTypes.func,
  options: PropTypes.array,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  errorMsg: PropTypes.string,
  closeDropDown: PropTypes.bool,
  elementNumber: PropTypes.number,
  closeOtherCallBack: PropTypes.func,
  required: PropTypes.bool,
}

export default DropDown
