import styled from "styled-components/macro"
export const SnackContainer = styled.div`
  display: flex;
  justify-content: center;
  position: fixed;
  width: 100%;
  bottom: 2rem;
  z-index: 3;
  animation: moveUp 500ms ease-in-out;

  @keyframes moveUp {
    0% {
      opacity: 0;
      transform: translateY(300%);
    }
    100% {
      opacity: 1;
      transform: translateY(0%);
    }
  }

  .snack-content {
    background-color: var(--hex-twenty);
    color: var(--hex-one);
    font-size: 1rem;
    padding: 0.5em 1.5em;
    box-shadow: 0px 4px 17px -5px rgba(var(--rgb-ten), var(--alpha-two));
    border-radius: 5px;

    &.unmount {
      animation: moveDown 600ms ease-in-out;
    }

    @keyframes moveDown {
      0% {
        opacity: 1;
        transform: translateY(0%);
      }
      100% {
        opacity: 0;
        transform: translateY(300%);
      }
    }
  }
`
