import ReactDOM from "react-dom"
import {useState, useEffect} from "react"
import useSnack from "hooks/useSnack"
import {SnackContainer} from "./styles"

const Snack = () => {
  const {snack, content, toggleSnack} = useSnack()
  const [unmount, setUnmount] = useState()
  useEffect(() => {
    if (snack) {
      const timeout = setTimeout(() => {
        setUnmount(true)
        const timer = setTimeout(() => {
          setUnmount(false)
          toggleSnack()
          clearTimeout(timer)
        }, 500)
        clearTimeout(timeout)
      }, 4000)
    }
  }, [snack]) //eslint-disable-line
  if (!snack) return null
  return ReactDOM.createPortal(
    <SnackContainer>
      <div className={`snack-content light ${unmount ? "unmount" : ""}`}>
        {content}
      </div>
    </SnackContainer>,
    document.querySelector("#snack-root"),
  )
}

export default Snack
