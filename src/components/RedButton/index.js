import {Button} from "./styles"

const index = ({children, disabled, onClick, style}) => {
  return (
    <Button disabled={disabled} onClick={onClick} style={style}>
      {children}
    </Button>
  )
}

export default index
