import styled from "styled-components/macro"

export const Button = styled.button`
  background-color: var(--hex-seven);
  color: var(--hex-one);
  padding: 0.6875em 1.8175em;
  padding-left: ${props => (props.style ? props.style.paddingSide : "")};
  padding-right: ${props => (props.style ? props.style.paddingSide : "")};
  border-radius: 1.4375em;
  font-size: 1rem;
  letter-spacing: 2px;

  &:disabled {
    background-color: var(--rgb-sixteen);
  }
`
