import ReactDOM from "react-dom"
import {Container} from "./styles"
import Spinner from "components/Loader/Spinner"
const PageLoader = ({show = false, children}) => {
  if (!show) return null
  return ReactDOM.createPortal(
    <Container>
      {children ? (
        children
      ) : (
        <div className="loader-content">
          <Spinner />
        </div>
      )}
    </Container>,
    document.querySelector("#page-loader-root"),
  )
}
export default PageLoader
