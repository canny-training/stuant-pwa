import styled, {keyframes} from "styled-components/macro"

const spin = keyframes`
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
`

const Container = styled.div`
  border: 4px solid var(--hex-seven);
  border-top: 4px solid transparent;
  border-radius: 50%;
  animation: ${spin} 1s linear infinite;
`

const Spinner = ({height = "2.5rem", width = "2.5rem"}) => (
  <Container style={{height, width}} />
)

export default Spinner
