import {useState, useRef, useEffect} from "react"

const CustomWheel = ({selected, data, type, onDateChange}) => {
  const [position, setPosition] = useState(selected ? -(selected - 1) * 50 : 0)
  let offset = useRef(0)
  let dragging = useRef(false)
  let previousY = useRef(0)
  const [style, setStyle] = useState({
    willChange: "transform",
    transition: `transform ${Math.abs(0) / 100 + 0.1}s`,
    transform: `translateY(${selected ? -(selected - 1) * 50 : 0}px)`,
  })

  useEffect(() => {
    let selectedPosition = -(selected - 1) * 50
    setStyle({
      willChange: "transform",
      transition: `transform ${Math.abs(offset.current) / 100 + 0.1}s`,
      transform: `translateY(${position}px)`,
    })
    if (!dragging.current && position !== selectedPosition) {
      setPosition(selectedPosition)
    }
  }, [position, data, type, onDateChange, selected])

  const onMouseDown = event => {
    previousY.current = event.touches ? event.touches[0].clientY : event.clientY
    dragging.current = true
    const elem = document.querySelector(".datepicker-wrapper")

    elem.addEventListener("mousemove", onMouseMove)
    elem.addEventListener("mouseup", onMouseUp)
    elem.addEventListener("touchmove", onMouseMove)
    elem.addEventListener("touchend", onMouseUp)
  }

  const onMouseMove = event => {
    let clientY = event.touches ? event.touches[0].clientY : event.clientY
    offset.current = clientY - previousY.current
    let maxPosition = -data.length * 50
    setPosition(prevPos =>
      Math.max(maxPosition, Math.min(50, prevPos + offset.current)),
    )
    previousY.current = event.touches ? event.touches[0].clientY : event.clientY
  }

  const onMouseUp = () => {
    let maxPosition = -(data.length - 1) * 50
    dragging.current = false
    let finalPosition = null
    setPosition(prevPos => {
      finalPosition = Math.max(
        maxPosition,
        Math.min(0, Math.round((prevPos + offset.current * 5) / 50) * 50),
      )
      return finalPosition
    })

    const elem = document.querySelector(".datepicker-wrapper")

    elem.removeEventListener("mousemove", onMouseMove)
    elem.removeEventListener("mouseup", onMouseUp)
    elem.removeEventListener("touchmove", onMouseMove)
    elem.removeEventListener("touchend", onMouseUp)

    onDateChange(type, -finalPosition / 50)
  }

  const renderElem = elem => <li key={elem}>{elem}</li>

  return (
    <div
      className="dragdealer year"
      onMouseDown={onMouseDown}
      onTouchStart={onMouseDown}
    >
      <ul className="handle" style={style}>
        {data.map(renderElem)}
      </ul>
    </div>
  )
}

export default CustomWheel
