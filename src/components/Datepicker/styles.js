import styled, {keyframes} from "styled-components/macro"

const datepickerIn = keyframes`
  0% {
    transform: translateY(-100%);
    opacity: 0;
  }
  100%{
    transform: translateY(0);
    opacity: 1;
  }
`

export const Container = styled.div`
  text-align: center;
  font-size: 1.25rem;
  height: 100vh;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  background: rgba(var(--rgb-ele), var(--alpha-six));
  backdrop-filter: blur(2px);
  position: fixed;
  z-index: 100;
  width: 100%;
  top: 0;
  left: 0;

  .datepicker-wrapper {
    width: 70%;
    height: 15.625rem;
    background-color: var(--hex-one);
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    border-radius: 4px;
    animation: ${datepickerIn} 0.5s ease-in-out;
  }

  .date-picker {
    display: flex;
    padding: 3.125rem 0;
    background: var(--hex-one);
    overflow: hidden;
    width: 100%;
    justify-content: space-evenly;
    align-items: center;
    margin: 0 auto;
  }
  .day,
  .month,
  .year {
    position: relative;
    height: 3.125rem;
    margin: 0px;
    border-top: 1px solid var(--hex-eight);
    border-bottom: 1px solid var(--hex-eight);
    border-radius: 0;
  }
  .day:before,
  .month:before,
  .year:before,
  .day:after,
  .month:after,
  .year:after {
    content: "";
    position: absolute;
    left: 0;
    width: 3.75rem;
    height: 3.125rem;
    background-color: var(--hex-one);
    opacity: 0.85;
    pointer-events: none;
    z-index: 1;
  }
  .day:before,
  .month:before,
  .year:before {
    top: -3.1875rem;
  }
  .day:after,
  .month:after,
  .year:after {
    bottom: -3.1875rem;
  }

  ul {
    margin: 0;
    padding: 0;
  }
  .day li,
  .month li,
  .year li {
    display: flex;
    justify-content: center;
    align-items: center;
    width: 3.75rem;
    height: 3.125rem;
    user-select: none;
  }

  .navigation {
    font-size: 0.875rem;
    width: 100%;
    display: flex;
    margin-top: 2.142em;
    justify-content: flex-end;
  }

  .navigation .cancel {
    color: var(--hex-seven);
    background-color: var(--hex-one);
  }

  .navigation .ok {
    width: 40%;
    text-align: center;
  }

  .ok .ok-btn {
    color: var(--hex-seven);
    background-color: var(--hex-one);
  }
`
