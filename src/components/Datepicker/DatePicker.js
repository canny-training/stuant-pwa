import {useState, useEffect} from "react"
import CustomWheel from "./CustomWheel"

const DatePicker = ({
  date,
  dob,
  setDob,
  onDateChange,
  allowFutureDate,
  setOpenDatepicker,
}) => {
  const MONTHS = [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "July",
    "Aug",
    "Sept",
    "Oct",
    "Nov",
    "Dec",
  ]

  const initDate = new Date()
  initDate.setFullYear(initDate.getFullYear() - 18)

  date = date ?? initDate
  const YEARS = new Array(initDate.getFullYear() - 1899)
    .fill(1900)
    .map((value, index) => value + index)

  const [currentDate, setCurrentDate] = useState(new Date(date))

  useEffect(() => {
    document.body.style.overflow = "hidden"
    return () => {
      document.body.style.overflow = "unset"
    }
  }, [])

  const dateChanged = (type, changedData) => {
    let newDate

    if (type === "day") {
      newDate = new Date(date.getFullYear(), date.getMonth(), changedData + 1)
    } else if (type === "month") {
      let maxDayInSelectedMonth
      if (
        initDate.getFullYear() === date.getFullYear() &&
        initDate.getMonth() <= changedData + 1
      ) {
        maxDayInSelectedMonth = initDate.getDate()
      } else {
        maxDayInSelectedMonth = new Date(
          date.getFullYear(),
          changedData + 1,
          0,
        ).getDate()
      }
      let day = Math.min(date.getDate(), maxDayInSelectedMonth)

      newDate = new Date(date.getFullYear(), changedData, day)
    } else if (type === "year") {
      let maxDayInSelectedMonth
      if (
        initDate.getFullYear() === 1900 + changedData &&
        initDate.getMonth() <= date.getMonth()
      ) {
        maxDayInSelectedMonth = initDate.getDate()
      } else {
        maxDayInSelectedMonth = new Date(
          1900 + changedData,
          date.getMonth() + 1,
          0,
        ).getDate()
      }
      let month
      if (initDate.getFullYear() === 1900 + changedData) {
        month = Math.min(date.getMonth(), initDate.getMonth())
      } else {
        month = date.getMonth()
      }
      let day = Math.min(date.getDate(), maxDayInSelectedMonth)
      newDate = new Date(1900 + changedData, month, day)
    }

    onDateChange(newDate)
    setCurrentDate(newDate)
  }

  let days
  if (
    initDate.getFullYear() === date.getFullYear() &&
    initDate.getMonth() === date.getMonth()
  ) {
    days = new Array(initDate.getDate())
      .fill(1)
      .map((value, index) => value + index)
  } else {
    days = new Array(
      new Date(date.getFullYear(), date.getMonth() + 1, 0).getDate(),
    )
      .fill(1)
      .map((value, index) => value + index)
  }

  let months
  if (initDate.getFullYear() !== date.getFullYear()) {
    months = MONTHS
  } else {
    months = MONTHS.slice(0, initDate.getMonth() + 1)
  }
  let years = YEARS

  const stopPropagation = e => e.stopPropagation()

  const handleCancel = () => {
    document.querySelector("body").style.overflow = "unset"
    setOpenDatepicker(false)
    if (dob !== "") {
      let parts = dob.split("-")
      onDateChange(new Date(parts[2] + "-" + parts[1] + "-" + parts[0]))
    }
  }

  const handleOk = () => {
    if (!allowFutureDate && currentDate > initDate) {
      return
    }
    let dat = new Date()
    if (allowFutureDate && currentDate <= dat.setDate(dat.getDate() - 1)) {
      return
    }

    let ye = new Intl.DateTimeFormat("en", {year: "numeric"}).format(
      currentDate,
    )
    let mo = new Intl.DateTimeFormat("en", {month: "2-digit"}).format(
      currentDate,
    )
    let da = new Intl.DateTimeFormat("en", {day: "2-digit"}).format(currentDate)

    setDob(`${da}-${mo}-${ye}`)
    document.querySelector("body").style.overflow = "unset"
    setOpenDatepicker(false)
  }

  return (
    <div className="datepicker-wrapper light" onClick={stopPropagation}>
      <div className="date-picker">
        <CustomWheel
          type="month"
          data={months}
          selected={date.getMonth() + 1}
          onDateChange={dateChanged}
        />
        <CustomWheel
          type="day"
          data={days}
          selected={date.getDate()}
          onDateChange={dateChanged}
        />
        <CustomWheel
          type="year"
          data={years}
          selected={date.getYear() + 1}
          onDateChange={dateChanged}
        />
      </div>
      <div className="navigation bold">
        <button className="cancel" onClick={handleCancel}>
          CANCEL
        </button>
        <div className="ok">
          <button className="ok-btn" onClick={handleOk}>
            OK
          </button>
        </div>
      </div>
    </div>
  )
}

export default DatePicker
