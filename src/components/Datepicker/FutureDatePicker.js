import {useState} from "react"
import CustomWheel from "./CustomWheel"

const FutureDatePicker = ({
  date,
  dob,
  setDob,
  onDateChange,
  allowFutureDate,
  setOpenDatepicker,
}) => {
  let startDate = new Date()
  let endDate = new Date(
    startDate.getFullYear() + 1,
    startDate.getMonth(),
    startDate.getDate(),
  )
  const MONTHS = [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "July",
    "Aug",
    "Sept",
    "Oct",
    "Nov",
    "Dec",
  ]
  const YEARS = new Array(2)
    .fill(new Date().getFullYear())
    .map((value, index) => value + index)
  const [currentDate, setCurrentDate] = useState(new Date(date))

  const dateChanged = (type, changedData) => {
    let newDate

    if (type === "day") {
      newDate = new Date(date.getFullYear(), date.getMonth(), days[changedData])
      if (newDate < startDate) newDate = startDate
      else if (newDate > endDate) newDate = endDate
    } else if (type === "month") {
      let maxDayInSelectedMonth = new Date(
        date.getFullYear(),
        MONTHS.indexOf(months[changedData + 1]),
        0,
      ).getDate()
      let day = Math.min(date.getDate(), maxDayInSelectedMonth)

      newDate = new Date(
        date.getFullYear(),
        MONTHS.indexOf(months[changedData]),
        day,
      )
      if (newDate < startDate) newDate = startDate
      else if (newDate > endDate) newDate = endDate
    } else if (type === "year") {
      let maxDayInSelectedMonth = new Date(
        new Date().getFullYear() + changedData,
        date.getMonth() + 1,
        0,
      ).getDate()
      let day = Math.min(date.getDate(), maxDayInSelectedMonth)

      newDate = new Date(
        new Date().getFullYear() + changedData,
        date.getMonth(),
        day,
      )
      if (newDate < startDate) newDate = startDate
      else if (newDate > endDate) newDate = endDate
    }

    onDateChange(newDate)
    setCurrentDate(newDate)
  }

  let daysArr = new Array(
    new Date(date.getFullYear(), date.getMonth() + 1, 0).getDate(),
  )
    .fill(1)
    .map((value, index) => value + index)
  let days = [...daysArr]
  if (date.getFullYear() === new Date().getFullYear()) {
    if (date.getMonth() === new Date().getMonth()) {
      days = daysArr.slice(daysArr.indexOf(new Date().getDate()))
    }
  } else if (date.getFullYear() === new Date().getFullYear() + 1) {
    if (date.getMonth() === new Date().getMonth()) {
      days = daysArr.slice(0, daysArr.indexOf(new Date().getDate()) + 1)
    }
  }
  let months
  if (date.getFullYear() === new Date().getFullYear()) {
    months = MONTHS.slice(new Date().getMonth())
  } else {
    months = MONTHS.slice(0, new Date().getMonth() + 1)
  }
  let years = YEARS

  const stopPropagation = e => e.stopPropagation()

  const handleCancel = () => {
    document.querySelector("body").style.overflow = "unset"
    setOpenDatepicker(false)
    if (dob !== "") {
      let parts = dob.split("-")
      onDateChange(new Date(parts[2] + "-" + parts[1] + "-" + parts[0]))
    }
  }

  const handleOk = () => {
    if (!allowFutureDate && currentDate > new Date()) {
      return
    }
    let dat = new Date()
    if (allowFutureDate && currentDate <= dat.setDate(dat.getDate() - 1)) {
      return
    }

    let ye = new Intl.DateTimeFormat("en", {year: "numeric"}).format(
      currentDate,
    )
    let mo = new Intl.DateTimeFormat("en", {month: "2-digit"}).format(
      currentDate,
    )
    let da = new Intl.DateTimeFormat("en", {day: "2-digit"}).format(currentDate)

    setDob(`${da}-${mo}-${ye}`)
    document.querySelector("body").style.overflow = "unset"
    setOpenDatepicker(false)
  }

  return (
    <div className="datepicker-wrapper light" onClick={stopPropagation}>
      <div className="date-picker">
        <CustomWheel
          type="month"
          data={months}
          selected={months.indexOf(MONTHS[date.getMonth()]) + 1}
          onDateChange={dateChanged}
        />
        <CustomWheel
          type="day"
          data={days}
          selected={days.indexOf(date.getDate()) + 1}
          onDateChange={dateChanged}
        />
        <CustomWheel
          type="year"
          data={years}
          selected={date.getYear() - new Date().getYear() + 1}
          onDateChange={dateChanged}
        />
      </div>
      <div className="navigation bold">
        <button className="cancel" onClick={handleCancel}>
          CANCEL
        </button>
        <div className="ok">
          <button className="ok-btn" onClick={handleOk}>
            OK
          </button>
        </div>
      </div>
    </div>
  )
}

export default FutureDatePicker
