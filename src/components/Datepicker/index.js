import DatePicker from "./DatePicker"
import FutureDatePicker from "./FutureDatePicker"
import {Container} from "./styles.js"

const MyDatePicker = ({
  date,
  onDateChange,
  openDatepicker,
  setOpenDatepicker,
  setDob,
  dob,
  allowFutureDate = false,
}) => {
  const closeDatepicker = () => {
    if (dob !== "") {
      let parts = dob.split("-")
      onDateChange(new Date(parts[2] + "-" + parts[1] + "-" + parts[0]))
    }
    document.querySelector("body").style.overflow = "unset"
    setOpenDatepicker(false)
  }

  if (!openDatepicker) return null

  return (
    <Container onClick={closeDatepicker}>
      {!allowFutureDate ? (
        <DatePicker
          date={date}
          onDateChange={onDateChange}
          setOpenDatepicker={setOpenDatepicker}
          setDob={setDob}
          dob={dob}
          allowFutureDate={allowFutureDate}
        />
      ) : (
        <FutureDatePicker
          date={date}
          onDateChange={onDateChange}
          setOpenDatepicker={setOpenDatepicker}
          setDob={setDob}
          dob={dob}
          allowFutureDate={allowFutureDate}
        />
      )}
    </Container>
  )
}

export default MyDatePicker
