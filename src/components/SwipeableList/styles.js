import styled from "styled-components/macro"

export const List = styled.div`
  flex: 1;
  width: 100%;
  height: 100%;
`
