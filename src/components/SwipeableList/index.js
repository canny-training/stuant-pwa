import {List} from "./styles"

const SwipeableList = ({children}) => {
  return <List>{children}</List>
}
export default SwipeableList
