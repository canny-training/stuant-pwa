import { useRef } from "react";
import {ListItem} from "./styles"

const SwipeableListItem = ({children}) => {
  // This way, we keep the component reusable, because any content
  // can be projected inside of the container.

  // DOM Refs
  const listElement = useRef(null)
  const wrapper = useRef(null)
  const background = useRef(null)

  const onDragStartMouse = evt => {
    window.addEventListener("mousemove", onMouseMove)
  }

  const onMouseMove = evt => {
    const left = evt.clientX - this.dragStartX
    if (left < 0) {
      this.left = left
    }
  }

  const onTouchMove = evt => {
    const touch = evt.targetTouches[0]
    const left = touch.clientX - this.dragStartX
    if (left < 0) {
      this.left = left
    }
  }

  const onDragStartTouch = evt => {
    window.addEventListener("touchmove", onTouchMove)
  }

  return (
    <ListItem>
      <div ref={wrapper} className="Wrapper">
        <div ref={background} className="Background">
          <span>Delete</span>
        </div>
        <div
          ref={listElement}
          className="ListItem"
          onMouseDown={onDragStartMouse}
          onTouchStart={onDragStartTouch}
        >
          {children}
        </div>
      </div>
    </ListItem>
  )
}
export default SwipeableListItem
