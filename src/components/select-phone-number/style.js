import styled from "styled-components"

export const Container = styled.div`
  margin: 1.25rem 1rem 0.5rem 1rem;
  .container {
    display: flex;
    align-items: center;
    border-bottom: 1px solid var(--hex-fourty-eight);
  }

  .flag {
    font-size: 1.5rem;
  }

  .wrapper {
    display: flex;
    align-items: center;
  }

  h4 {
    color: var(--hex-eight);
    line-height: 1.5rem;
    letter-spacing: 2px;
    margin: 0 0.5rem;
    white-space: nowrap;
  }

  .arrow {
    opacity: var(--alpha-six);
  }

  input {
    line-height: 1.5rem;
    letter-spacing: 2px;
    font-size: 1rem;
    align-self: stretch;
    width: calc(100vw - 10rem);
    padding: 0.5rem 0 0.5rem 1rem;
    color: var(--hex-eight);
    border: none;
    :focus {
      outline: none;
    }
  }
`
export const ModalContainer = styled.div`
  background-color: var(--hex-one);
  overflow-y: hidden;
  border-bottom-left-radius: 0.5rem;
  border-bottom-right-radius: 0.5rem;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  height: 85vh;

  .search-box {
  padding: 0 1rem;
  height: 2rem;
  border: 1px solid var(--hex-fourty-nine);
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin: 1.5rem;
  border-radius: 2rem;
}

input {
    line-height: 1.5rem;
    letter-spacing: 2px;
    align-self: stretch;
    width: calc(100vw - 5rem);
    padding: 0.5rem 0 0.5rem 1rem;
    color: var(--hex-eight);
    border: none;
    font-size: 0.75rem;
    :focus {
      outline: none;
    }
  }

.search-icon {
  width: 1rem;
  height: 1rem;
  fill-opacity: var(--alpha-six ));
}

.close {
  width: 1.5rem;
  height: 1.5rem;
}

.no-result {
  padding: 0.75rem;
  font-size: 0.75rem;
  color: var(--hex-fifty);
  line-height: 1.5rem;
  margin: 0.25rem;
  text-align: center;
}

.selected {
  background-color: var(--hex-twenty-seven);
}

.checkbox {
  position: absolute;
  right: 1rem;
}

ul {
  margin: 0;
  padding: 0;

  &::-webkit-scrollbar {
      width: 0.25rem;
    }
    &::-webkit-scrollbar-thumb {
      background-color: var(--hex-fourteen);
      border-radius: 0.25rem;
    }

    &:focus {
      outline: none;
    }
}
  li {
    display: flex;
    align-items: center;
    padding: 0.75rem 1.5rem;
    border-bottom: 1px solid var(--hex-fifty-one);
  }

  .phone-code {
    white-space: nowrap;
    margin: 0 0.5rem;
  }

  .country {
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
    margin: 0 0.5rem;
  }
`
