import {useState} from "react"
import {Container, ModalContainer} from "./style"
import CountriesArray from "./countries"
import ic_search from "assets/icons/country_search.svg"
import ic_close from "assets/icons/close.svg"
import {ReactComponent as Arrow} from "assets/icons/arrow_down.svg"
import CheckBoxOrder from "components/checkbox-order"
import useBottomSheet from "hooks/useBottomSheet"

const SelectPhoneNumber = ({onChangeNumber, phoneData = {}}) => {
  const {hideSheetContentWithAnimation, setSheetContent} = useBottomSheet()
  const {selCountry, formatted_phone} = phoneData

  const onChangeMobile = e => {
    try {
      const value = e.target.value.trim().match(/\d/g)?.join("") ?? ""
      const fullPhoneNumber = `${selCountry.countryCode}${value ?? ""}`
      const canEnterNumber =
        selCountry.countryCode === "91"
          ? value.length <= 10
          : fullPhoneNumber.length <= 15
      if (canEnterNumber && ((value && value.match(/^[0-9]+$/)) || !value)) {
        const isValidNumber =
          selCountry.countryCode === "91"
            ? value.length === 10
            : fullPhoneNumber.length >= 7 && fullPhoneNumber.length <= 15
        onChangeNumber({
          phone: value,
          formatted_phone: value,
          isValid: isValidNumber,
        })
      }
    } catch (error) {
      console.log("Phone number formatting error: ", error)
    }
  }

  const onChangeCountry = selectedItem => {
    onChangeNumber(selectedItem)
    hideSheetContentWithAnimation()
  }

  const toggleDropdown = () =>
    setSheetContent(
      [<SearchCountries selCountry={selCountry} onSelect={onChangeCountry} />],
      true,
    )

  const {countryCode, flag} = selCountry ?? {}
  return (
    <Container>
      <div className="container">
        <div className="wrapper" onClick={toggleDropdown}>
          <label className="flag">{flag}</label>
          <h4>+{countryCode}</h4>
          <Arrow className="arrow" fillOpacity="var(--alpha-six)" />
        </div>
        <input
          placeholder="MOBILE NUMBER"
          value={formatted_phone ?? ""}
          inputMode="numeric"
          type="numeric"
          onChange={onChangeMobile}
        />
      </div>
    </Container>
  )
}

export default SelectPhoneNumber

const SearchCountries = ({selCountry, onSelect}) => {
  const [countries, setCountries] = useState(CountriesArray)
  const [searchText, setSearchText] = useState("")

  const onChangeSearch = e => {
    const value = e.target.value
    const sText = value.toLowerCase().trim()
    const fCountries = CountriesArray.filter(
      it =>
        it.name.toLowerCase().includes(sText) ||
        it.id.toLowerCase().includes(sText) ||
        `+${it.countryCode}`.toLowerCase().includes(sText),
    )
    setSearchText(value)
    setCountries(fCountries)
  }

  const onChangeCountry = item =>
    onSelect({
      selCountry: item,
      phone: "",
      formatted_phone: "",
      isValid: false,
    })

  const clearInput = () => {
    setSearchText("")
    setCountries(CountriesArray)
  }

  const renderCountries = (item, index) => {
    const {countryCode, flag, name} = item ?? {}
    const selected =
      countryCode === selCountry.countryCode && name === selCountry.name
    return (
      <li
        key={index}
        className={selected ? "selected" : ""}
        onClick={onChangeCountry.bind(null, item)}
      >
        <label className="flag">{flag}</label>
        <h4 className="phone-code">+{countryCode}</h4>
        <h4 className="light country">{name}</h4>
        {selected && (
          <div className="checkbox">
            <CheckBoxOrder checked={selected} />
          </div>
        )}
      </li>
    )
  }
  return (
    <ModalContainer>
      <div className="search-box">
        <img src={ic_search} alt="search" />
        <input
          className="light"
          placeholder="Search"
          value={searchText}
          onChange={onChangeSearch}
        />
        {searchText.length > 0 && (
          <img
            className="close"
            src={ic_close}
            alt="close"
            onClick={clearInput}
          />
        )}
      </div>
      {countries.length <= 0 ? (
        <span className="no-result">No Result Found</span>
      ) : (
        <ul>{countries.map(renderCountries)}</ul>
      )}
    </ModalContainer>
  )
}
