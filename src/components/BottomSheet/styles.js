import styled, {keyframes} from "styled-components"

const moveUp = keyframes`
  0% {
    opacity: 0;
    transform: translateY(100%);
  }
  100% {
    opacity: 1;
  }
`

const moveDown = keyframes`
  0% {
    opacity: 1;
  }
  100% {
    opacity: 0;
    transform: translateY(100%);
  }
`

const overlayAnimationIncoming = keyframes`
  0%{
    background-color: rgba(var(--rgb-fifteen), var(--aplha-one));
  }
  100%{
    background-color: rgba(5, 5, 5, var(--alpha-six));
  }
`

const overlayAnimationOutgoing = keyframes`
  0%{
    background-color: rgba(var(--rgb-fifteen), var(--alpha-six));
  }
  100%{
    background-color: rgba(var(--rgb-fifteen), var(--aplha-one));
  }
`

export const Container = styled.div`
  .sheet-container {
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    z-index: 2;
    background-color: rgba(var(--rgb-fifteen), var(--alpha-six));
    backdrop-filter: blur(3px);
    animation: ${overlayAnimationIncoming} 0.7s ease-in;
  }

  .sheet-container.unmount {
    background-color: rgba(var(--rgb-ten), var(--aplha-one));
    animation: ${overlayAnimationOutgoing} 0.5s ease-out;
  }

  .sheet {
    position: absolute;
    bottom: 0;
    width: 100%;
    animation: ${moveUp} 0.5s ease-in;
    transition: height 1s ease-in-out;
    @media (max-height: 450px) {
      height: 80%;
    }
  }

  .sheet-content {
    -ms-overflow-style: none;
    scrollbar-width: none;
    background-color: var(--hex-one);
    height: 100%;
    overflow-y: scroll;
    border-top-left-radius: 20px;
    border-top-right-radius: 20px;
    margin-top: 0.75rem;
    min-height: ${props => props.minHeight || "0"};

    @media (max-height: 450px) {
      min-height: unset;
    }
  }

  .sheet-content::-webkit-scrollbar {
    display: none;
  }

  .sheet-handle-container {
    width: 100%;
  }

  .sheet-handle {
    width: 15%;
    height: 0.25rem;
    border-radius: 2px;
    background-color: var(--hex-one);
    margin: 0 auto;
  }

  .go-out {
    animation: ${moveDown} 0.5s ease-out;
  }
`
