import {useEffect} from "react"
import ReactDOM from "react-dom"
import {Container} from "./styles"

const BottomSheet = ({
  show,
  setShow,
  unmount,
  setUnmount,
  closable,
  showSheetHandle,
  hideSheetContent,
  minHeight,
  children,
  onClose,
}) => {
  useEffect(() => {
    const close = e => {
      if (e.keyCode === 27 && closable) {
        setUnmount(true)
        let timeout = setTimeout(() => {
          hideSheetContent(onClose)
          clearTimeout(timeout)
        }, 400)
      }
    }
    window.addEventListener("keydown", close)
    return () => {
      window.removeEventListener("keydown", close)
    }
  }, [closable, setShow, setUnmount, hideSheetContent]) //eslint-disable-line

  useEffect(() => {
    if (show) document.body.style.overflow = "hidden"
    else document.body.style.overflow = "unset"
  }, [show])

  useEffect(() => {
    return () => {
      document.body.style.overflow = "unset"
    }
  }, [])

  useEffect(() => {
    var xDown = null
    var yDown = null

    const getTouches = evt => {
      return evt.touches || evt.originalEvent.touches
    }

    const handleTouchStart = evt => {
      const firstTouch = getTouches(evt)[0]
      xDown = firstTouch.clientX
      yDown = firstTouch.clientY
    }

    const handleTouchMove = evt => {
      if (!xDown || !yDown) {
        return
      }

      let xUp = evt.touches[0].clientX
      let yUp = evt.touches[0].clientY

      let xDiff = xDown - xUp
      let yDiff = yDown - yUp

      if (Math.abs(xDiff) > Math.abs(yDiff)) {
        if (xDiff > 0) {
          return
        } else {
          return
        }
      } else {
        if (yDiff > 0) {
          return
        } else {
          if (closable) {
            setUnmount(true)
            let timeout = setTimeout(() => {
              hideSheetContent(onClose)
              clearTimeout(timeout)
            }, 400)
          }
        }
      }
      xDown = null
      yDown = null
    }

    document.addEventListener("touchstart", handleTouchStart, false)
    document.addEventListener("touchmove", handleTouchMove, false)

    return () => {
      document.removeEventListener("touchstart", handleTouchStart)
      document.removeEventListener("touchmove", handleTouchMove)
    }
  }, [closable, setShow, setUnmount, hideSheetContent]) //eslint-disable-line

  const stopPropagation = event => {
    event.stopPropagation()
  }

  const unMountWithTimer = () => {
    if (closable) {
      setUnmount(true)
      let timeout = setTimeout(() => {
        hideSheetContent(onClose)
        clearTimeout(timeout)
      }, 400)
    }
  }

  if (show === true && children) {
    return ReactDOM.createPortal(
      <Container minHeight={minHeight}>
        <div
          className={`sheet-container ${unmount && "unmount"}`}
          onClick={unMountWithTimer}
        >
          <div
            className={`sheet ${unmount && "go-out"}`}
            onTouchStart={stopPropagation}
            onTouchMove={stopPropagation}
          >
            {showSheetHandle && (
              <div className="sheet-handle-container">
                <div className="sheet-handle"></div>
              </div>
            )}
            <div className="sheet-content" onClick={stopPropagation}>
              {children}
            </div>
          </div>
        </div>
      </Container>,
      document.querySelector("#bottom-sheet-root"),
    )
  } else {
    return null
  }
}

export default BottomSheet
