import styled from "styled-components"
export const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  height: 100vh;

  h2 {
    line-height: 1.875rem;
    font-size: 1.375rem;
    margin: 0.75rem 2rem;
  }

  span {
    color: var(--hex-oslo-grey);
    margin: 0.75rem 2rem;
  }

  .page-indicator {
    display: flex;
    justify-content: center;
    margin: 3rem;
  }

  .filled,
  .dot {
    width: 0.5rem;
    height: 0.5rem;
    border-radius: 50%;
    margin: 0.25rem;
    background-color: var(--hex-alto);
  }
  .filled {
    background-color: var(--hex-theme);
  }
`
export const Footer = styled.div`
  margin: 3rem 2rem;
`
