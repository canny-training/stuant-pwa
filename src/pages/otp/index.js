import {PAGE_SPLASH} from "constants/RouteConstants"
import {useState} from "react"
import {useHistory} from "react-router-dom"
import usePageLoader from "hooks/usePageLoader"
import {Container} from "./styles"
import useAxios from "hooks/useAxios"
import {URL_LOGIN_TOKEN} from "constants/api-constants"

const OTP = () => {
  const [otp, setOTP] = useState("")
  const {showPageLoader, hidePageLoader} = usePageLoader()
  const history = useHistory()
  const axios = useAxios()
  const onChange = e => {
    const value = e.target.value
    if (value.length <= 6) setOTP(value)
  }

  const verifyOTP = async () => {
    try {
      showPageLoader()
      const result = await window.confirmationResult.confirm(otp)
      console.log(result?._tokenResponse?.idToken)
      const response = await axios({
        method: "get",
        url: URL_LOGIN_TOKEN,
        headers: {Authorization: `Bearer ${result?._tokenResponse?.idToken}`},
      })
      console.log(response)
      history.replace(PAGE_SPLASH)
    } catch (error) {
      console.log(error)
    } finally {
      hidePageLoader()
    }
  }
  const onClick = () => {
    if (otp.length === 6) verifyOTP()
  }
  return (
    <Container>
      <input onChange={onChange} value={otp} />
      <button onClick={onClick}>Verify OTP</button>
    </Container>
  )
}

export default OTP
