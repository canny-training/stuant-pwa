import styled from "styled-components"
import ic_logo from "assets/icons/ic_logo.svg"

export const MainContiner = styled.div`
  background-color: yellow;
  height: 100vh;

  .wrapper {
    padding: 1rem;
  }
`
export const Container = styled.div`
  .bg {
    background: url(${ic_logo});
  }
`
