import {useEffect} from "react"
import {Container} from "./styles"
import ic_splash from "assets/icons/ic_splash.svg"
import {useHistory} from "react-router-dom"
import {PAGE_LANDING} from "constants/RouteConstants"
import useLogin from "network/useLoginAPI"

const Splash = () => {
  const history = useHistory()
  const {loginWithFirebase} = useLogin()
  useEffect(() => {
    loginWithFirebase()
      .then(res => {
        console.log("RESPONSE", res)
      })
      .catch(err => {
        console.log("ERROR", err)
      })
    setTimeout(() => history.push(PAGE_LANDING), 3000)
  }, []) //eslint-disable-line
  return (
    <Container>
      <img src={ic_splash} alt="" />
    </Container>
  )
}

export default Splash
