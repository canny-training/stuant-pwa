import {PAGE_OTP} from "constants/RouteConstants"
import {useState, useEffect} from "react"
import {useHistory} from "react-router-dom"
import usePageLoader from "hooks/usePageLoader"
import {Container} from "./styles"
import {getAuth, RecaptchaVerifier, signInWithPhoneNumber} from "firebase/auth"

const Login = () => {
  const auth = getAuth()
  const {showPageLoader, hidePageLoader} = usePageLoader()
  useEffect(() => {
    auth.languageCode = auth.useDeviceLanguage()
    window.recaptchaVerifier = new RecaptchaVerifier(
      "sign-in-button",
      {
        size: "invisible",
        callback: response => {
          // reCAPTCHA solved, allow signInWithPhoneNumber.
        },
      },
      auth,
    )
  }, []) //eslint-disable-line
  const [phoneNo, setPhoneNo] = useState("")
  const history = useHistory()
  const onChange = e => setPhoneNo(e.target.value)
  const onClick = async () => {
    showPageLoader()
    try {
      const appVerifier = window.recaptchaVerifier
      const phoneNumber = `+91${phoneNo}`
      const result = await signInWithPhoneNumber(auth, phoneNumber, appVerifier)
      window.confirmationResult = result
      history.push({pathname: PAGE_OTP, state: phoneNumber})
    } catch (error) {
      console.error("ERROR in Login Page: ", error)
    } finally {
      hidePageLoader()
    }
  }
  return (
    <Container>
      <input id="sign-in-button" onChange={onChange} value={phoneNo} />
      <button onClick={onClick}>Get OTP</button>
    </Container>
  )
}

export default Login
