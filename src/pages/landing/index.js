import {useState, useEffect} from "react"
import {useHistory} from "react-router-dom"
import useFooter from "hooks/useFooter"
import ic_tour_extra from "assets/icons/ic_tour_extra.svg"
import ic_tour_parent from "assets/icons/ic_tour_parent.svg"
import ic_tour_scientific from "assets/icons/ic_tour_scientific.svg"
import ic_tour_self from "assets/icons/ic_tour_self.svg"
import ic_tour_student from "assets/icons/ic_tour_student.svg"
import {Container, Footer} from "./styles"
import {PAGE_DASHBOARD} from "constants/RouteConstants"

const Landing = () => {
  const [index, setIndex] = useState(0)
  const history = useHistory()
  const {toggleFooter} = useFooter()
  useEffect(() => {
    toggleFooter({
      custom: (
        <Footer>
          <button onClick={onClick}>
            {index === tourContents.length - 1 ? "Get Started" : "Next"}
          </button>
        </Footer>
      ),
    })
    return () => toggleFooter()
  }, [index]) //eslint-disable-line
  const tourContents = [
    {
      title: "Any platform to identify my kid’s potential / Talent?",
      desc: "STUANT mobile app will help you to identify your kids potential from various dimensions",
      icon: ic_tour_student,
    },
    {
      title: "Parental observation",
      desc: "Analyze your kids under different categories from your observation.",
      icon: ic_tour_parent,
    },
    {
      title: "Self Assessment",
      desc: "Your kids can continuously evaluate their progress on Academics, GK, Reasoning, etc.",
      icon: ic_tour_self,
    },
    {
      title: "Scientific Assessment",
      desc: "Identify your kid's strengths and weaknesses based on a proven approach.",
      icon: ic_tour_scientific,
    },
    {
      title: "Extra Curricular",
      desc: "Maintain Records of your Achievements and Interests",
      icon: ic_tour_extra,
    },
  ]

  const onClick = () =>
    index === tourContents.length - 1
      ? history.push(PAGE_DASHBOARD)
      : setIndex(() => index + 1)

  const renderIndicator = (_, position) => (
    <div key={position} className={index === position ? "filled" : "dot"} />
  )

  const {title, desc, icon} = tourContents[index]
  return (
    <Container>
      <img src={icon} alt="" />
      <h2>{title}</h2>
      <span>{desc}</span>
      <div className="page-indicator">{tourContents.map(renderIndicator)}</div>
    </Container>
  )
}

export default Landing
