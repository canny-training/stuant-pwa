import {useState} from "react"
import ModalContext from "contexts/ModalContext"
import Modal from "components/modal"

const useModalProvider = () => {
  const [modal, setModal] = useState(false)
  const [content, setContent] = useState("")

  const toggleModal = (data = false) => {
    setModal(!!data)
    data && setContent(data)
  }
  return {modal, content, toggleModal}
}

const ModalProvider = ({children}) => {
  const value = useModalProvider()

  return (
    <ModalContext.Provider value={value}>
      {children}
      <Modal />
    </ModalContext.Provider>
  )
}

export default ModalProvider
