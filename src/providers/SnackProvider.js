import {useState} from "react"
import SnackContext from "contexts/SnackContext"
import Snack from "components/snack"

const useSnackProvider = () => {
  const [snack, setSnack] = useState(false)
  const [content, setContent] = useState("")

  const toggleSnack = (content = "") => {
    setSnack(!!content)
    setContent(content)
  }

  return {snack, content, toggleSnack}
}

const SnackProvider = ({children}) => {
  const value = useSnackProvider()

  return (
    <SnackContext.Provider value={value}>
      {children}
      <Snack />
    </SnackContext.Provider>
  )
}

export default SnackProvider
