import {Fragment} from "react"
import HeaderProvider from "./HeaderProvider"
import BottomSheetProvider from "./BottomSheetProvider"
import ModalProvider from "./ModalProvider"
import FooterProvider from "./FooterProvider"
import PageLoaderProvider from "./PageLoaderProvider"
import SnackProvider from "./SnackProvider"
import OfflineProvider from "./OfflineProvider"
import UserProvider from "./UserProvider"
const AppProviders = ({children}) => (
  <Fragment>
    <UserProvider>
      <PageLoaderProvider>
        <HeaderProvider>
          <FooterProvider>
            <ModalProvider>
              <SnackProvider>
                <OfflineProvider>
                  <BottomSheetProvider>{children}</BottomSheetProvider>
                </OfflineProvider>
              </SnackProvider>
            </ModalProvider>
          </FooterProvider>
        </HeaderProvider>
      </PageLoaderProvider>
    </UserProvider>
  </Fragment>
)
export default AppProviders
