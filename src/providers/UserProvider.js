import {UserContext} from "contexts/UserContexts"
import {useState} from "react"
import {getAuth} from "firebase/auth"

const useProviderAuth = () => {
  const [userData, setUser] = useState({
    access_token: "test",
    user: {},
    metaData: {},
  })
  const {access_token} = userData
  const updateUser = data => setUser(prev => ({...prev, ...data}))
  const isSignedIn = !!access_token && !!getAuth().currentUser
  return {
    userData,
    updateUser,
    isSignedIn,
  }
}
const UserProvider = props => {
  const userValue = useProviderAuth()
  return (
    <UserContext.Provider value={userValue}>
      {props.children}
    </UserContext.Provider>
  )
}

export default UserProvider
