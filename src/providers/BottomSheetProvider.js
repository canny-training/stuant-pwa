import {useState, useCallback, Fragment} from "react"
import BottomSheetContext from "contexts/BottomSheetContext"
import BottomSheet from "components/BottomSheet"

const BottomSheetProvider = props => {
  const [values, setValues] = useState({
    unmount: false,
    show: false,
    closable: false,
    showSheetHandle: true,
    contentRef: [],
    minHeight: 0,
    onClose: () => {},
  })

  const setShow = show => {
    setValues({...values, show})
  }

  const setUnmount = unmount => {
    setValues({...values, unmount})
  }

  const setClosable = closable => {
    setValues({...values, closable})
  }

  const setSheetContent = useCallback(
    (
      content,
      closable = false,
      showSheetHandle = true,
      minHeight = 0,
      callback,
      onClose,
    ) => {
      setValues({
        contentRef: content,
        closable,
        showSheetHandle,
        show: true,
        unmount: false,
        minHeight,
        onClose,
      })
      if (callback && typeof callback === "function") callback()
    },
    [],
  )

  const hideSheetContent = useCallback(callback => {
    setValues({
      unmount: false,
      show: false,
      closable: false,
      showSheetHandle: true,
      contentRef: [],
      minHeight: 0,
      onClose: () => {},
    })
    if (callback && typeof callback === "function") callback()
  }, [])

  const hideSheetContentWithAnimation = useCallback(
    callback => {
      setValues(values => ({...values, unmount: true}))
      let timeout = setTimeout(() => {
        hideSheetContent(callback)
        clearTimeout(timeout)
      }, 400)
    },
    [hideSheetContent],
  )

  const bottomSheet = {
    setUnmount,
    setShow,
    setClosable,
    setSheetContent,
    hideSheetContent,
    hideSheetContentWithAnimation,
    ...values,
  }

  return (
    <BottomSheetContext.Provider value={bottomSheet}>
      {props.children}
      <BottomSheet {...bottomSheet}>
        {values.contentRef.length > 0 &&
          values.contentRef.map((elem, i) => (
            <Fragment key={i}>{elem}</Fragment>
          ))}
      </BottomSheet>
    </BottomSheetContext.Provider>
  )
}

export default BottomSheetProvider
