import {useState, useCallback} from "react"
import PageLoaderContext from "contexts/PageLoaderContext"
import PageLoader from "components/Loader/PageLoader"

const PageLoaderProvider = ({children}) => {
  const [show, setShow] = useState(false)
  const [loaderContent, setLoaderContent] = useState([])

  const showPageLoader = useCallback((content = []) => {
    setShow(true)
    setLoaderContent(content)
  }, [])

  const hidePageLoader = useCallback(() => {
    setShow(false)
    setLoaderContent([])
  }, [])

  const value = {
    show,
    showPageLoader,
    hidePageLoader,
  }

  return (
    <PageLoaderContext.Provider value={value}>
      {children}
      <PageLoader show={show}>
        {loaderContent.length &&
          loaderContent.map((content, i) => <div key={i}>{content}</div>)}
      </PageLoader>
    </PageLoaderContext.Provider>
  )
}

export default PageLoaderProvider
