import {useState} from "react"
import FooterContext from "contexts/FooterContext"
import Footer from "components/footer"

const useProvider = () => {
  const [footer, setFooter] = useState(false)
  const [content, setContent] = useState("")
  const toggleFooter = (data = false) => {
    setFooter(!!data)
    if (data) {
      setContent(data)
    }
  }
  return {footer, content, toggleFooter}
}
const FooterProvider = ({children}) => {
  const value = useProvider()
  return (
    <FooterContext.Provider value={value}>
      {children}
      <Footer />
    </FooterContext.Provider>
  )
}
export default FooterProvider
