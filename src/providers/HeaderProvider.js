import {useState} from "react"
import HeaderContext from "contexts/HeaderContext"
import Header from "components/header"

const useProvider = () => {
  const [header, setHeader] = useState(false)
  const [content, setContent] = useState("")
  const toggleHeader = (data = false) => {
    setHeader(data)
    data && setContent(data)
  }
  return {header, content, toggleHeader}
}
const HeaderProvider = ({children}) => {
  const value = useProvider()
  return (
    <HeaderContext.Provider value={value}>
      {children}
      <Header />
    </HeaderContext.Provider>
  )
}

export default HeaderProvider
