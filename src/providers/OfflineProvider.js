import {useState} from "react"
import OfflineContext from "contexts/OfflineContext"
import Offline from "components/offline"

const useSnackProvider = () => {
  const [offline, setOffline] = useState(false)

  return {offline, setOffline}
}

const SnackProvider = ({children}) => {
  const value = useSnackProvider()

  return (
    <OfflineContext.Provider value={value}>
      {!value.offline && children}
      <Offline />
    </OfflineContext.Provider>
  )
}

export default SnackProvider
