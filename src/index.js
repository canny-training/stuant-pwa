import {StrictMode} from "react"
import ReactDOM from "react-dom"
import {BrowserRouter, Route} from "react-router-dom"
import App from "./App"
import * as serviceWorkerRegistration from "./serviceWorkerRegistration"
import AppProviders from "providers"
import "./index.css"
import {PAGE_SPLASH} from "constants/RouteConstants"
import {initializeApp} from "firebase/app"

const {REACT_APP_ENV} = process.env

const config =
  REACT_APP_ENV === "development"
    ? {
        apiKey: "AIzaSyDKpUqFME3Jakwt7uFxPb1BodDaVq9_mR4",
        authDomain: "stuant-f4c84.firebaseapp.com",
        databaseURL: "https://stuant-f4c84.firebaseio.com",
        projectId: "stuant-f4c84",
        storageBucket: "stuant-f4c84.appspot.com",
        messagingSenderId: "32095113662",
        appId: "1:32095113662:web:9fd54f1162f4372079a9ef",
        measurementId: "G-XGYEB40MYK",
      }
    : REACT_APP_ENV === "production"
    ? {}
    : {}

initializeApp(config)

// const messaging = firebase.messaging()

// messaging.setBackgroundMessageHandler(function (payload) {
//   const notificationTitle = payload.data.title
//   const notificationOptions = {
//     body: payload.data.body,
//     icon: "./public/assets/manifest/x48.png",
//   }
//   return self.registration.showNotification(
//     notificationTitle,
//     notificationOptions,
//   )
// })

// self.addEventListener("notificationclick", event => {
//   return event
// })

if (document && document.documentElement) {
  document.documentElement.setAttribute("data-theme", "light")
}
ReactDOM.render(
  <StrictMode>
    <BrowserRouter>
      <AppProviders>
        <Route path={PAGE_SPLASH} component={App} />
      </AppProviders>
    </BrowserRouter>
  </StrictMode>,
  document.getElementById("app-root"),
)
serviceWorkerRegistration.register({
  onUpdate: registration => {
    console.log("On register", registration) // eslint-disable-line
    const waitingServiceWorker = registration.waiting
    if (waitingServiceWorker) {
      waitingServiceWorker.addEventListener("statechange", event => {
        if (event.target.state === "activated") {
          window.location.reload()
        }
      })
      console.log("Post message to skip waiting stage") // eslint-disable-line
      waitingServiceWorker.postMessage({type: "SKIP_WAITING"})
    }
  },
})
