code --install-extension 2gua.rainbow-brackets
code --install-extension eamodio.gitlens
code --install-extension ban.spellright
code --install-extension bierner.github-markdown-preview
code --install-extension bierner.markdown-checkbox
code --install-extension bierner.markdown-emoji
code --install-extension bierner.markdown-preview-github-styles
code --install-extension bierner.markdown-yaml-preamble
code --install-extension dbaeumer.vscode-eslint
code --install-extension esbenp.prettier-vscode
code --install-extension formulahendry.auto-rename-tag
code --install-extension ionutvmi.path-autocomplete
code --install-extension JamesBirtles.svelte-vscode
code --install-extension jasonnutter.search-node-modules
code --install-extension letrieu.expand-region
code --install-extension mgmcdermott.vscode-language-babel
code --install-extension mikestead.dotenv
code --install-extension RoscoP.ActiveFileInStatusBar
code --install-extension sdras.night-owl
code --install-extension silvenon.mdx
code --install-extension WallabyJs.quokka-vscode
code --install-extension wayou.vscode-todo-highlight
code --install-extension xyz.local-history
code --install-extension vscode-styled-components
